<?php
add_shortcode( 'themeum_resume', 'themeum_resume_function');

function themeum_resume_function($atts, $content = null) {
	
	$heading 	= '';
	$count_post = '';
	$class  	= '';

	extract(shortcode_atts(array(
		'heading' 		=> '',
    	'count_post' 	=>	6,		
		'class' 		=> '',
		), $atts));


	global $wpdb;
  	global $post;

  	$args = array(
      'post_type' => 'time_line',
      'order' => 'ASC',
      'posts_per_page' => esc_attr($count_post)
    );

  	$timeline = new WP_Query($args);

	$output = '';
    $output .= '<div class="row pos-reletive">';
    $output .= '<div class="timeline-row"></div>';

    $output .= '<div class="col-md-6 col-xs-6">';
    $output .= '<div class="timeline-left-content text-right">';

    $i=0;
  	if ( $timeline->have_posts() ){
		while($timeline->have_posts()) {
			$timeline->the_post();
			$subtitle = get_post_meta(get_the_ID(),'themeum_timeline_subtitle',true);
			$date = get_post_meta(get_the_ID(),'themeum_timeline_date',true);

            if($i%2 == 0){
                $output .= '<div class="timeline-description">';
                    $output .= '<h6>'.get_the_title().'</h6>';
                    $output .= '<p>'.$subtitle.'</p>';
                $output .= '</div>';
            }    
            if($i%2 == 1){
                $output .= '<div class="timeline-date">';
                    $output .= '<p>'.$date.'</p>';
                $output .= '</div>';
            }
            $i++;

            /* get_the_title(); | $subtitle; | $date;*/
		}//End of while
	}//End of IF
    $output .= '</div>';
    $output .= '</div>';
    
    $output .= '<div class="col-md-6 col-xs-6">';
    $output .= '<div class="timeline-right-content text-left">';
    $i=0;
    if ( $timeline->have_posts() ){
        while($timeline->have_posts()) {
            $timeline->the_post();
            $subtitle = get_post_meta(get_the_ID(),'themeum_timeline_subtitle',true);
            $date = get_post_meta(get_the_ID(),'themeum_timeline_date',true);
            
            if($i%2 == 1){
                $output .= '<div class="timeline-description">';
                    $output .= '<h6>'.get_the_title().'</h6>';
                    $output .= '<p>'.$subtitle.'</p>';
                $output .= '</div>';
            }    
            if($i%2 == 0){
                $output .= '<div class="timeline-date">';
                    $output .= '<p>'.$date.'</p>';
                $output .= '</div>';
            }
            $i++;

        }//End of while
    }//End of IF
    $output .= '</div>';
    $output .= '</div>';

    $output .='</div>';         

	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Political Resume", 'themeum-core'),
		"base" => "themeum_resume",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Political Resume", 'themeum-core'),
		"category" => esc_html__('Politist', 'themeum-core'),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => esc_html__("Heading", 'themeum-core'),
				"param_name" => "heading",
				"value" => "",
				),		

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'themeum-core'),
				"param_name" => "class",
				"value" => "",
				),

			)

		));
}