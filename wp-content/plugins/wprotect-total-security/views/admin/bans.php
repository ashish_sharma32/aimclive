<?php

    if(!defined('_CHKINC')){
        die;
    }

    $create_nonce = wp_create_nonce( $ajax_nonce );
    $create_nonce_status = wp_create_nonce( $ajax_nonce_status );

?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('Banned IP address', 'wprotect') ); ?>
    </h1>

    <?php echo $tab; ?>
    
    <div class="mainContainer banip">
        
        <div class="grid-container">
            
            <div class="grid-12 module-item  grid-item">
                
                <div class="box-item">
                    <div class="inside">
                        
                            <div class="grid-container">

                                <div class="grid-6 grid-item ban-tools">
                                    <form id="wprotect-add-ban-ip" method="post" action="admin.php?page=<?php echo $textdomain; ?>&tab=ban">
                                 <?php wp_nonce_field( $form_nonce, $textdomain.'_save_option_nonce' ); ?>
                                        <div class="list-item">

                                            <div class="grid-container">
                                                    
                                                <div class="grid-item grid-6">
                                                    
                                                    <span class="form-label"><?php _e('Ban IP address', 'wprotect'); ?>:</span>
                                                    <span class="form-label-info"></span>

                                                </div>

                                                <div class="grid-item grid-6">
                                                    
                                                    <input type="text" id="add_ip" data-validetta="required" class="form-input-text" name="add_ip" placeholder="<?php _e( 'Example', 'wprotect'); ?>: 255.255.255.255" />

                                                </div>

                                            </div>

                                        </div>

                                        <div class="list-item" align="left">
                                            
                                            <button type="submit" class="form-submit nomargin"><i class="dashicons dashicons-yes"></i> <?php _e('Add IP', 'wprotect'); ?></button>

                                        </div>
                                    </form>
                                </div>

                                <div class="grid-6 grid-item ban-tools">
                                <form id="wprotect-config" method="post" action="admin.php?page=<?php echo $textdomain; ?>&tab=ban">
                                 <?php wp_nonce_field( $form_nonce, $textdomain.'_save_option_nonce' ); ?>
                                 <?php
                                    $key = 'banip';
                                    $status = $saved_options[$key] == 'on' ? true : false;

                                ?>    
                                        <div class="list-item protection-status">

                                            <span class="status-item label"><?php _e('Enable/Disable', 'wprotect'); ?>:</span>
                                            <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                                           
                                        </div>  

                                        <div class="list-item">
                                            
                                            <div class="grid-container">
                                                
                                                <div class="grid-item grid-4">
                                                    
                                                    <span class="form-label"><?php _e('Ban "hour(s)"', 'wprotect'); ?>:</span>
                                                    <span class="form-label-info"></span>

                                                </div>

                                                <div class="grid-item grid-4">
                                                    
                                                    <input type="text" id="ban_time" value="<?php echo esc_html(@$options['ban_time']); ?>" data-validetta="required,number" class="form-input-text" name="ban_time" />

                                                </div>

                                                <div class="grid-item grid-4" align="right">

                                                    <button type="submit" class="form-submit nomargin"><i class="dashicons dashicons-yes"></i> <?php _e('Save settings', 'wprotect'); ?></button>

                                                </div>

                                            </div>

                                        </div>  

                                    </form>

                                </div>

                            </div>
                        
                    </div>
                </div>

                <div class="block-seperator"></div>

                <div class="box-item">
                    <div class="inside">
                        <table class="table-list bans">
                        <thead>
                            <tr>
                                <th align="center" width="10">#</th>
                                <th width="60%" class="table-title"><?php _e('IP Address', 'wprotect'); ?></th>
                                <th class="hidden-mobile"><?php _e('Started', 'wprotect'); ?></th>
                                <th class="hidden-mobile"><?php _e('End date', 'wprotect'); ?></th>
                                <th><?php _e('Action', 'wprotect'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $d = 0;
                            for($i=0,$n=count($datas['rows']);$i<$n;$i++):
                                $row = $datas['rows'][$i];
                                $remove_url = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=ban';
                                $check_url = 'http://whatismyipaddress.com/ip/'.$row->ip_address;
                        ?>
                            <tr>
                                <td align="center"><?php echo $datas['offset']+$d+1; ?></td>
                                <td><b><?php echo esc_html($row->ip_address); ?></b> <a href="<?php echo $check_url; ?>" target="_blank"><span class="dashicons dashicons-info"></span> <?php _e('IP Lookup', 'wprotect'); ?></a></td>
                                <td align="center"><?php echo date('Y/m/d H:i', $row->created_at); ?></td>
                                <td align="center" class="color-warning"><?php echo date('Y/m/d H:i', $row->end_time); ?></td>
                                <td align="center">
                                    <a href="#" class="ban-btn color-danger" data-id="<?php echo $row->ID; ?>" class="remove-ban"><span class="dashicons dashicons-no"></span><?php _e('Remove ban', 'wprotect'); ?></a>
                                </td>
                            </tr>
                        <?php
                                $d++;
                            endfor;
                        ?>
                        </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="pagination">
                    <?php echo $datas['pagination']; ?>
                </div>

            </div>                      
            
        </div>
         
    </div>
    
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){

        jQuery('.ban-btn').click(function(e){

            e.preventDefault();

            var $this = jQuery(this);
            var $id = $this.data('id');

            var data = { 
                    'action': '<?php echo $ajax_method; ?>',
                    'id': $id,
                    'nonce': '<?php echo $create_nonce; ?>'
            };

            if(confirm('<?php _e('Are you sure to delete this ip from the ban list?', 'wprotect'); ?>')){

                jQuery.post(ajaxurl, data, function(responce){
                    
                    if(responce.result){
                        
                        $this.parent().parent('tr').remove();
                        
                    }else{
                        
                        alert('Sorry, please try again!');
                        
                    }
                    
                }, 'json');

            }

        });

        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

      jQuery('#toggle-<?php echo $key; ?>').toggles({
            on: <?php echo (int)$status; ?>,
            width: 60,
        });
        
        jQuery('#toggle-<?php echo $key; ?>').on('toggle', function(e, active) {
            
            var $this = jQuery(this);
            var $protection = $this.data('protection-type');
            var $action = active ? 'on' : 'off';
            
            var data = { 
                    'action': '<?php echo $ajax_method_status; ?>',
                    'protection': $protection,
                    'status': $action,
                    'nonce': '<?php echo $create_nonce_status; ?>'
            };
            
            jQuery.post(ajaxurl, data, function(responce){
                
                if(responce.result){
                    
                    if(responce.status == 'on'){
                        
                        toastr.success( responce.text );
                        
                    }else{
                        
                        toastr.warning( responce.text );
                        
                    }
                    
                }else{
                    
                    alert('<?php _e( 'Sorry, please try again!', 'wprotect'); ?>');
                    
                }
                
            }, 'json');
            
        });

        jQuery("#wprotect-config").validetta({
            bubblePosition: 'bottom',
            realTime : true
        });

        jQuery("#wprotect-add-ban-ip").validetta({
            bubblePosition: 'bottom',
            realTime : true
        });

    });
</script>