<?php
    if(!defined('_CHKINC')){
        die;
    }
?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('Protection logs', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer logs">
        
        <div class="grid-container">
            
            <div class="grid-12 module-item grid-item">
                
                <div class="box-item">
                    <div class="inside">

                            <table class="table-list logs">
                            <thead>
                                <tr>
                                    <th align="center" width="10">#</th>
                                    <th width="40%" class="table-title"><?php _e('Log message', 'wprotect'); ?></th>
                                    <th class="hidden-mobile"><?php _e('Created', 'wprotect'); ?></th>
                                    <th class="hidden-mobile"><?php _e('Owner', 'wprotect'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $d = 0;
                                for($i=0,$n=count($datas['rows']);$i<$n;$i++):
                                    $row = $datas['rows'][$i];
                                    $icon_style = ganjargal_logActionStyle( $row->log_type, $row->log_action );
                                    $username = intval($row->user_id) == 0 ? _WPROTECT_TEXTDOMAIN : $row->user_login;
                            ?>
                                <tr>
                                    <td align="center"><?php echo $datas['offset']+$d+1; ?></td>
                                    <td><span class="icon <?php echo @$icon_style['style']; ?>"><i class="dashicons <?php echo @$icon_style['icon']; ?>"></i></span> <b><?php echo esc_html($row->log_message); ?></b></td>
                                    <td align="center" class="hidden-mobile"><?php echo date('Y/m/d H:i', $row->created_at); ?></td>
                                    <td align="center" class="hidden-mobile"><b><?php echo $username; ?></b></td>
                                </tr>
                            <?php
                                    $d++;
                                endfor;
                            ?>
                            </tbody>
                            </table>
                            
                    </div>
                </div>
                
                <div class="pagination">
                    <?php echo $datas['pagination']; ?>
                </div>

            </div>                      
            
        </div>
         
    </div>
    
</div>
