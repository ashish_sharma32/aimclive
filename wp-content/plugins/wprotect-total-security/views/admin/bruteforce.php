<?php

    if(!defined('_CHKINC')){
        die;
    }

    $create_nonce = wp_create_nonce( $ajax_nonce );
    $create_nonce_username = wp_create_nonce( $ajax_nonce_username );

?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">

    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('Blocking Brute Force attacks', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer bruteforce">
        
        <div class="grid-container">
            
            <div class="grid-6 center-block module-item grid-item">
                
                <div class="box-item">
                    <div class="inside">
                        
                        <form id="wprotect-config" method="post" action="admin.php?page=<?php echo _WPROTECT_TEXTDOMAIN; ?>&tab=bruteforce">
                            
                            <?php wp_nonce_field( $form_nonce, $textdomain.'_save_option_nonce' ); ?>

                            <?php
                                $key = 'bruteforce';
                                $create_nonce = wp_create_nonce( $ajax_nonce );
                                $status = $saved_options[$key] == 'on' ? true : false;
                            ?>
                            
                            <div class="list-item protection-status">

                                <span class="status-item label"><?php _e('Enable/Disable', 'wprotect'); ?>:</span>
                                <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                               
                            </div>

                            <?php
                                if(isset($alerts['bruteforce'])):
                            ?>
                                <div class="list-item">
                                    
                                    <div class="grid-container">
                                        
                                        <div class="grid-item grid-6">
                                            
                                            <span class="form-label"><?php _e('Change default "admin" username', 'wprotect'); ?>:</span>
                                            <span class="form-label-info">
                                                <?php _e('"admin" username is exists!', 'wprotect'); ?>
                                            </span>

                                        </div>

                                        <div class="grid-item grid-6">
                                            
                                            <input type="text" id="username_admin" class="form-input-text" name="username_admin" data-validetta="required,minLength[5],maxLength[60],remote[check_username]" />
                                            <span class="form-label-info">
                                                <?php _e('After changing your username, it will be logout automatically. Please login using new username.', 'wprotect'); ?>
                                            </span>
                                        </div>

                                    </div>

                                </div>
                                <div class="form-zone"><span></span></div>
                            <?php
                                endif;
                            ?>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Maximium number of login attempts', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"> If you exceed maximium number of login attempts, it will show captcha or ip address will be blocked automatically. </span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" id="attempts_limit" value="<?php echo esc_html(@$options['attempts_limit']); ?>" data-validetta="required,number" class="form-input-text" name="attempts_limit" />

                                    </div>

                                </div>

                            </div>
                           
                            <div class="form-zone" id="recaptcha"><em><?php _e('Google reCaptcha configuration', 'wprotect'); ?>:</em><span></span></div>
                            
                            <div class="form-helper">
                                <a href="https://developers.google.com/recaptcha/docs/start" target="_blank"><?php _e('How to get the public and private keys for Google reCAPTCHA?', 'wprotect'); ?></a>
                            </div>
                            
                            <div class="form-helper info">
                                <?php _e('If Google reCAPTCHA keys are not configured, brute-force attacking ip address will be banned automatically.', 'wprotect'); ?>
                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Site key', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" id="attempts_limit" value="<?php echo esc_html(@$options['recaptcha_site_key']); ?>" class="form-input-text" name="recaptcha_site_key" />

                                    </div>

                                </div>

                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Secret key', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" id="recaptcha_secret_key" value="<?php echo esc_html(@$options['recaptcha_secret_key']); ?>" class="form-input-text" name="recaptcha_secret_key" />

                                    </div>

                                </div>

                            </div>
                            
                            <p align="center">
                                <button type="submit" class="form-submit"><i class="dashicons dashicons-yes"></i> <?php _e('Save settings', 'wprotect'); ?></button>
                            </p>

                        </form>

                    </div>

                </div>
                
                <div class="block-seperator"></div>

                <div class="box-item">
                    <div class="inside">
                        
                        <div class="view-info">

                            <h2 class="box-title color-info"><?php _e('Information of Brute-force attack', 'wprotect'); ?>:</h2>

                            <?php _e('In cryptography, a brute-force attack consists of an attacker trying many passwords or passphrases with the hope of eventually guessing correctly. The attacker systematically checks all possible passwords and passphrases until the correct one is found.', 'wprotect'); ?>

                            <a href="https://en.wikipedia.org/wiki/Brute-force_attack" target="_blank"><?php _e('Read more ...', 'wprotect'); ?></a>

                        </div>

                    </div>

                </div>
                
            </div>                      
            
        </div>
         
    </div>
    
</div>
<script type="text/javascript">
    
    var $ = jQuery.noConflict();

    jQuery(document).ready(function(){
        
        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

      jQuery('#toggle-<?php echo $key; ?>').toggles({
            on: <?php echo (int)$status; ?>,
            width: 60,
        });
        
        jQuery('#toggle-<?php echo $key; ?>').on('toggle', function(e, active) {
            
            var $this = jQuery(this);
            var $protection = $this.data('protection-type');
            var $action = active ? 'on' : 'off';
            
            var data = { 
                    'action': '<?php echo $ajax_method; ?>',
                    'protection': $protection,
                    'status': $action,
                    'nonce': '<?php echo $create_nonce; ?>'
            };
            
            jQuery.post(ajaxurl, data, function(response){
                
                if(response.result){
                    
                    if(response.status == 'on'){
                        
                        toastr.success( response.text );
                        
                    }else{
                        
                        toastr.warning( response.text );
                        
                    }
                    
                }else{
                    
                    alert('<?php _e( 'Sorry,please try again!', 'wprotect'); ?>');
                    
                }
                
            }, 'json');
            
        });

        jQuery("#wprotect-config").validetta({
            bubblePosition: 'bottom',
            realTime : true,
            validators: {
                remote : {
                    check_username : {
                        type : 'POST',
                        url : ajaxurl,
                        data: {
                            'action' : '<?php echo $ajax_method_username; ?>',
                            'nonce': '<?php echo $create_nonce_username; ?>'
                        },
                        datatype : 'json'
                    }
                }
            }
        });
        
    });
    
</script>