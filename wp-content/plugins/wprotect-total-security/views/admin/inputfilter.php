<?php

    if(!defined('_CHKINC')){
        die;
    }

    $create_nonce = wp_create_nonce( $ajax_nonce );

?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('Filter input against XSS/Injection attacks', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer inputfilter">
        
        <div class="grid-container">
            
            <div class="grid-6 center-block module-item grid-item">
                
                <div class="box-item">
                    <div class="inside">
                        
                        <form id="wprotect-config" method="post" action="admin.php?page=<?php echo _WPROTECT_TEXTDOMAIN; ?>&tab=inputfilter">
                            
                            <?php wp_nonce_field( $form_nonce, _WPROTECT_TEXTDOMAIN.'_save_option_nonce' ); ?>

                            <?php
                                $key = 'inputfilter';
                                $status = $saved_options[$key] == 'on' ? true : false;

                            ?>
                            
                            <div class="list-item protection-status">

                                <span class="status-item label"><?php _e('Enable/Disable', 'wprotect'); ?>:</span>
                                <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                               
                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">

                                    <div class="grid-item grid-4">
                                        <span class="form-label"><label><input type="checkbox" name="filter_get" id="filter_get" value="1" <?php echo ganjargal_checkIt($options['filter_get']); ?> /> <?php _e('Filter GET requests', 'wprotect'); ?></label></span>
                                    </div>

                                    <div class="grid-item grid-4">
                                        <span class="form-label"><label><input type="checkbox" name="filter_post" id="filter_post" value="1" <?php echo ganjargal_checkIt($options['filter_post']); ?> /> <?php _e('Filter POST requests', 'wprotect'); ?></label></span>
                                    </div>

                                    <div class="grid-item grid-4">
                                        <span class="form-label"><label><input type="checkbox" name="filter_cookies" id="filter_cookies" value="1" <?php echo ganjargal_checkIt($options['filter_cookies']); ?> /> <?php _e('Filter COOKIES requests', 'wprotect'); ?></label></span>
                                    </div>

                                </div>

                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-4">
                                        <span class="form-label"><label><input type="checkbox" name="enable_xss" id="enable_xss" value="1" <?php echo ganjargal_checkIt($options['enable_xss']); ?> /> <?php _e('Enable', 'wprotect'); ?> <a href="https://en.wikipedia.org/wiki/Cross-site_scripting" target="_blank">XSS</a> <?php _e('filter', 'wprotect'); ?></label></span>
                                    </div>

                                    <div class="grid-item grid-4">
                                        <span class="form-label"><label><input type="checkbox" name="filter_file_inclusion" id="filter_file_inclusion" value="1" <?php echo ganjargal_checkIt($options['filter_file_inclusion']); ?> /> <?php _e('File inclusion filter', 'wprotect'); ?></label></span>
                                    </div>

                                </div>

                            </div>

                            <p align="center">
                                <button type="submit" class="form-submit"><i class="dashicons dashicons-yes"></i> <?php _e('Save settings', 'wprotect'); ?></button>
                            </p>

                        </form>

                    </div>

                </div>
                
                <div class="block-seperator"></div>

                <div class="box-item">
                    <div class="inside">
                        
                        <div class="view-info">

                            <h2 class="box-title color-info"><?php _e('What is Input-filter protection?', 'wprotect'); ?></h2>
                            <?php _e('Input protection module will check all kind of vulnerability including cross-site scripting, local file inclusion, remote file inclusion, sql injection from GET, POST, Cookie requests and block them.', 'wprotect'); ?>
                            
                        </div>

                    </div>

                </div>
                
            </div>                      
            
        </div>
         
    </div>
    
</div>
<script type="text/javascript">
                
    jQuery(function(){
        
        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

      jQuery('#toggle-<?php echo $key; ?>').toggles({
            on: <?php echo (int)$status; ?>,
            width: 60,
        });
        
        jQuery('#toggle-<?php echo $key; ?>').on('toggle', function(e, active) {
            
            var $this = jQuery(this);
            var $protection = $this.data('protection-type');
            var $action = active ? 'on' : 'off';
            
            var data = { 
                    'action': '<?php echo $ajax_method; ?>',
                    'protection': $protection,
                    'status': $action,
                    'nonce': '<?php echo $create_nonce; ?>'
            };
            
            jQuery.post(ajaxurl, data, function(responce){
                
                if(responce.result){
                    
                    if(responce.status == 'on'){
                        
                        toastr.success( responce.text );
                        
                    }else{
                        
                        toastr.warning( responce.text );
                        
                    }
                    
                }else{
                    
                    alert('<?php _e( 'Sorry! Try again!', 'wprotect'); ?>');
                    
                }
                
            }, 'json');
            
        });
        
    });
    
</script>