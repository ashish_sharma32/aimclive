<?php

    if(!defined('_CHKINC')){
        die;
    }

?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('WProtect - Settings', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer settings">
        
        <div class="grid-container">
            
            <div class="grid-6 center-block module-item grid-item">
               
                <div class="box-item">
                    <div class="inside">
                        
                        <form id="wprotect-config" method="post" action="admin.php?page=<?php echo _WPROTECT_TEXTDOMAIN; ?>&tab=settings">

                            <?php wp_nonce_field( $form_nonce, _WPROTECT_TEXTDOMAIN.'_save_option_nonce' ); ?>
                                                        
                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Alert notification recieve email', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">

                                        <?php 

                                            $email = $extra_options['alert_email'];
                                            $users = get_users('role=administrator'); 

                                            if($email == ''&& count($users)){

                                                $email = $users[0]->user_email;

                                            }

                                        ?>
                                        
                                        <input type="text" id="alert_email" value="<?php echo esc_html($email); ?>" data-validetta="required,email" class="form-input-text" name="alert_email" />

                                    </div>

                                </div>

                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Theme', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <select class="form-select" name="admin_theme" id="admin_theme">
                                            <?php
                                                foreach ($admin_themes as $theme) {
                                                    $selected = $theme == $extra_options['theme_style'] ? 'selected=selected' : '';
                                            ?>
                                            <option value="<?php echo $theme; ?>" <?php echo $selected; ?>><?php echo ucfirst($theme); ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>

                                    </div>

                                </div>

                            </div>

                            <div class="form-zone" id="onesignal"><em><?php _e('Mobile & Desktop notification (Onesignal.com)', 'wprotect'); ?>:</em><span></span></div>

                            <?php
                                if(ganjargal_curlSupport()):
                            ?>

                            <div class="form-helper">
                                <a href="https://documentation.onesignal.com/docs/web-push-sdk-setup-http" target="_blank"><?php _e('How to get the App_ID and API_Key for OneSignal.com?', 'wprotect'); ?></a>
                            </div>
                            
                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('App ID', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" id="onesignal_app_id" value="<?php echo esc_html(@$extra_options['onesignal_app_id']); ?>" class="form-input-text" name="onesignal_app_id" />

                                    </div>

                                </div>

                            </div>
                            
                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('API key', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" id="onesignal_api_key" value="<?php echo esc_html(@$extra_options['onesignal_api_key']); ?>" class="form-input-text" name="onesignal_api_key" />

                                    </div>

                                </div>

                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('SubDomain', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" id="onesignal_subdomain" value="<?php echo esc_html(@$extra_options['onesignal_subdomain']); ?>" class="form-input-text" name="onesignal_subdomain" />

                                    </div>

                                </div>

                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Safari Web ID', 'wprotect'); ?>:</span>
                                        <span class="form-label-info"></span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" id="onesignal_safari_webid" value="<?php echo esc_html(@$extra_options['onesignal_safari_webid']); ?>" class="form-input-text" name="onesignal_safari_webid" />

                                    </div>

                                </div>

                            </div>

                            <?php
                                else:
                            ?>
                            <div class="alert-item warning">
                                <?php _e( 'CURL is not supported on your server! So "OneSignal Notification Service" is not possible!', 'wprotect' ); ?>
                            </div>
                            <?php
                                endif;
                            ?>

                            <p align="center">
                                <button type="submit" class="form-submit"><i class="dashicons dashicons-yes"></i> <?php _e('Save settings', 'wprotect'); ?></button>
                            </p>

                        </form>

                    </div>
                </div>
                
            </div>                      
            
        </div>
         
    </div>
    
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){

        jQuery("#wprotect-config").validetta({
            realtime: true,
            bubblePosition: 'bottom'
        });

    });
</script>