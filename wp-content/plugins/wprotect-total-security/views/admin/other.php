<?php

    if(!defined('_CHKINC')){
        die;
    }

    $create_nonce = wp_create_nonce( $ajax_nonce );
    $create_nonce_setstatus = wp_create_nonce( $ajax_nonce_setstatus );

?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('Other protections', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer other">
        
        <div class="grid-container">
            
            <div class="grid-6 center-block module-item grid-item">
                
                <div class="box-item">
                    <div class="inside">
                        
                        <?php
                            $protection = 'upload_phpkill';
                        ?>

                        <h2 class="box-title" id="<?php echo $protection; ?>"><?php _e('Prevent PHP execution in "WP-Content/Uploads" directory', 'wprotect'); ?> <a href="javascript:;" class="tooltip tooltip-anchor" title="<?php _e('The uploads directory is the one directory that will almost need to be writable by the web server. It\'s where all files are uploaded remotely. The protection system prevents to run the malware, exploit, shell files in the this directory.', 'wprotect'); ?>"><i class="dashicons dashicons-editor-help"></i></a></h2>
                        
                        <?php

                            $class = $protection_directories->checkUploadDirectory() ? '' : 'hidden';
                            $class2 = $protection_directories->checkUploadDirectory() ? 'hidden' : '';

                        ?>
                            <p align="center" class="<?php echo $protection; ?> <?php echo $class; ?> color-success"><i class="dashicons dashicons-yes"></i><?php _e('Prevented PHP execution in this directory!', 'wprotect'); ?>
                            </p>
                        
                            <p align="center">
                                <a href="#" data-protection="<?php echo $protection; ?>" class="ajax-protection-request form-submit normal <?php echo $class2; ?>"><i class="dashicons dashicons-warning"></i> <?php _e('Prevent it', 'wprotect'); ?></a>
                            </p>
                        

                    </div>

                </div>

                <div class="block-seperator"></div>

                    <div class="box-item">
                        <div class="inside">
                            
                            <h2 class="box-title" id="disallowfileedit"><?php _e('Disallow file edit', 'wprotect'); ?> <a href="javascript:;" class="tooltip tooltip-anchor" title="<?php _e('It is recommended to disable file editing within the WordPress dashboard.', 'wprotect'); ?>"><i class="dashicons dashicons-editor-help"></i></a></h2>
                            
                            <?php
                                $key = 'disallowfileedit';
                                $status_disallowfileedit = $saved_options[$key] == 'on' ? true : false;
                            ?>

                            <div class="list-item protection-status">

                                <span class="status-item label"><?php _e('Enable/Disable', 'wprotect'); ?>:</span>
                                <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                               
                            </div>

                        </div>
                    </div>
                
                <div class="block-seperator"></div>

                <div class="box-item">
                    <div class="inside">
                        
                        <?php
                            $protection = 'directory_listing';
                        ?>

                        <h2 class="box-title" id="<?php echo $protection; ?>"><?php _e('Disable directory listing', 'wprotect'); ?> <a href="javascript:;" class="tooltip tooltip-anchor" title="<?php _e('By default when your web server does not find an index file (i.e. a file like index.php or index.html), it automatically displays an index page showing the contents of the directory. This could make your site vulnerable to hack attacks by revealing important information needed to exploit a vulnerability in a WordPress plugin, theme, or your server in general. In this article, we will show you how to disable directory browsing in WordPress.', 'wprotect'); ?>"><i class="dashicons dashicons-editor-help"></i></a></h2>
                        
                        <?php

                            $class = $protection_directories->checkRootHtaccess() ? '' : 'hidden';
                            $class2 = $protection_directories->checkRootHtaccess() ? 'hidden' : '';

                        ?>
                            <p align="center" class="<?php echo $protection; ?> <?php echo $class; ?> color-success"><i class="dashicons dashicons-yes"></i> <?php _e('Disabled directory listing! ', 'wprotect'); ?>
                            </p>
                        
                            <p align="center">
                                <a href="#" data-protection="<?php echo $protection; ?>" class="ajax-protection-request form-submit normal <?php echo $class2; ?>"><i class="dashicons dashicons-warning"></i> <?php _e('Disable it', 'wprotect'); ?></a>
                            </p>
                        

                    </div>

                </div>

                <div class="block-seperator"></div>
                
                <div class="box-item">
                    <div class="inside">
                        
                        <?php
                            $protection = 'robots';
                        ?>

                        <h2 class="box-title" id="<?php echo $protection; ?>"><?php _e('Protecting against Google Hacking Database', 'wprotect'); ?> <a href="javascript:;" class="tooltip tooltip-anchor" title="<?php _e('The protection system protects against hackers to find your wordpress and plugins using search term of the search engines.', 'wprotect'); ?>"><i class="dashicons dashicons-editor-help"></i></a></h2>
                        
                        <?php

                            $class = $protection_directories->checkRobots() ? '' : 'hidden';
                            $class2 = $protection_directories->checkRobots() ? 'hidden' : '';

                        ?>
                            <p align="center" class="<?php echo $protection; ?> <?php echo $class; ?> color-success"><i class="dashicons dashicons-yes"></i> <?php _e('Protected against Google Hacking Database!', 'wprotect'); ?>
                            </p>
                        
                            <p align="center">
                                <a href="#" data-protection="<?php echo $protection; ?>" class="ajax-protection-request form-submit normal <?php echo $class2; ?>"><i class="dashicons dashicons-warning"></i> <?php _e('Protect it', 'wprotect'); ?></a>
                            </p>
                        

                    </div>

                </div>
                
            </div>                      
            
        </div>
         
    </div>
    
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){

        jQuery('.ajax-protection-request').click(function(e){

            e.preventDefault();

            var $this = jQuery(this);
            var $protection = $this.data('protection');

            var data = { 
                    'action': '<?php echo $ajax_method; ?>',
                    'protection': $protection,
                    'nonce': '<?php echo $create_nonce; ?>'
            };

            jQuery.post(ajaxurl, data, function(response){
                
                if(response.result){
                    
                    $this.addClass('hidden');
                    jQuery('.'+$protection).removeClass('hidden');
                    
                }else{
                    
                    alert(response.message);
                    
                }
                
            }, 'json');


        });

        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

        jQuery('#toggle-disallowfileedit').toggles({
            on: <?php echo (int)$status_disallowfileedit; ?>,
            width: 60,
        });
        
        jQuery('#toggle-disallowfileedit').on('toggle', function(e, active) {
            
            var $this = jQuery(this);
            var $protection = $this.data('protection-type');
            var $action = active ? 'on' : 'off';
            
            var data = { 
                    'action': '<?php echo $ajax_method_setstatus; ?>',
                    'protection': $protection,
                    'status': $action,
                    'nonce': '<?php echo $create_nonce_setstatus; ?>'
            };
            
            jQuery.post(ajaxurl, data, function(responce){
                
                if(responce.result){
                    
                    if(responce.status == 'on'){
                        
                        toastr.success( responce.text );
                        
                    }else{
                        
                        toastr.warning( responce.text );
                        
                    }
                    
                }else{
                    
                    alert('<?php _e( 'Sorry! Try again!', 'wprotect'); ?>');
                    
                }
                
            }, 'json');
            
        });

        jQuery('.tooltip').tooltipster({
            theme: 'tooltipster-borderless',
            maxWidth: 200,
            side: 'right'
        });
              
});
</script>