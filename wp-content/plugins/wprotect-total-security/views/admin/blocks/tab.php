<?php
    if(!defined('_CHKINC')){
        die;
    }
?>

<nav class="nav-tab-wrapper" id="nav-tab-selector">
    <?php    
        for($i=0,$n=count($items);$i<$n;$i++){    

            if(empty($active)){

                $active = $items[0]['tabid'];

            }

            $item = $items[$i];
            $class = $active == $item['tabid'] ? ' nav-tab-active' : '';
    ?>
    <a href="admin.php?page=<?php echo $item['menu_slug']; ?>&tab=<?php echo $item['tabid']; ?>" class="nav-tab<?php echo $class; ?>"><?php echo $item['menu_title']; ?>
        <?php if ($item['tabid'] == 'alerts' && intval($alerts) > 0): ?>
            <?php
                $alert_class = intval($alerts) < 10 ? '' : 'double';
            ?>
            <span class="alert <?php echo $alert_class; ?>"><?php echo intval($alerts); ?></span>
        <?php endif; ?>
    </a>
    <?php
        }
    ?>
</nav>

<select id="nav-selector" class="hidden">
    <?php    
        for($i=0,$n=count($items);$i<$n;$i++){    

            if(empty($active)){

                $active = $items[0]['tabid'];

            }

            $item = $items[$i];
            $selected = $active == $item['tabid'] ? 'selected=selected' : '';
    ?>
    <option <?php echo $selected; ?> value="admin.php?page=<?php echo $item['menu_slug']; ?>&tab=<?php echo $item['tabid']; ?>"><?php echo $item['menu_title']; ?></option>
    <?php
        }
    ?>
</select>