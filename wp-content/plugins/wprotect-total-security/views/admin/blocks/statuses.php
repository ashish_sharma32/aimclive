<?php
    if(!defined('_CHKINC')){
        die;
    }
?>
<div class="box-item">
    <div class="inside">

        <h2 class="box-title color-success">
            <?php _e('Protections status', 'wprotect'); ?>:
        </h2>

        <form id="wprotect-config">
                
            <script type="text/javascript">
                
                jQuery(function(){
                    
                    toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                    
                });
                
            </script>
            
                <?php
                
                    $create_nonce = wp_create_nonce( $ajax_nonce );

                    foreach($protect_types as $key=>$item){

                        if($item['is_live'] == true){

                        $status = $saved_options[$key] == 'on' ? true : false;
                        $check_box_class = 'check_'.$key;
                ?>

                <div class="list-item">

                    <span class="status-item label"><?php _e( $item['title'], $textdomain ); ?>:</span>
                    <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                    
                    <script type="text/javascript">
                        
                        jQuery(function(){
                            
                            jQuery('#toggle-<?php echo $key; ?>').toggles({
                                on: <?php echo (int)$status; ?>,
                                width: 60,
                            });
                            
                            jQuery('#toggle-<?php echo $key; ?>').on('toggle', function(e, active) {
                                
                                var $this = jQuery(this);
                                var $protection = $this.data('protection-type');
                                var $action = active ? 'on' : 'off';
                                
                                var data = { 
                                        'action': '<?php echo $ajax_method; ?>',
                                        'protection': $protection,
                                        'status': $action,
                                        'nonce': '<?php echo $create_nonce; ?>'
                                };
                                
                                jQuery.post(ajaxurl, data, function(response){
                                    
                                    if(response.result){
                                        
                                        if(response.status == 'on'){
                                            
                                            toastr.success( response.text );
                                            
                                        }else{
                                            
                                            toastr.warning( response.text );
                                            
                                        }
                                        
                                    }else{
                                        
                                        alert('<?php _e( 'Sorry, please try again!', 'wprotect'); ?>');
                                        
                                    }
                                    
                                }, 'json');
                                
                            });
                            
                         });
                         
                    </script>
                    
                </div>

                <?php

                        }
                    }
                ?>

        </form>
    </div>
</div>