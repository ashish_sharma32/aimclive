<?php
    if(!defined('_CHKINC')){
        die;
    }
?>
<div class="box-item">
    <div class="inside">
        <h2 class="box-title color-info">
            <?php _e('Latest logs', 'wprotect'); ?>
        </h2>

		<?php
			if(count($logs['rows'])):
		?>
        <div id="log-scroller">
    		<ul class="list-items log">
            <?php
                for($i=0,$n=count($logs['rows']);$i<$n;$i++):
                    $row = $logs['rows'][$i];
                    $icon_style = ganjargal_logActionStyle( $row->log_type, $row->log_action );
            ?>
                <li>
                    <span class="icon <?php echo @$icon_style['style']; ?>"><i class="dashicons <?php echo @$icon_style['icon']; ?>"></i></span>
                    <span class="date"><?php echo date('Y/m/d H:i', $row->created_at); ?></span>
                    <span class="msg"><?php echo $row->log_message; ?></span>
                </li>
            <?php
                endfor;
            ?>
            </ul>
        </div>
        <script type="text/javascript">
            jQuery(function(){
                jQuery('#log-scroller').slimScroll({
                    height: '184px'
                });
            });
        </script>
		<?php
			else:
		?>
			<?php _e( 'Currently, there is not any stored logs.', 'wprotect' ); ?>
		<?php
			endif;
		?>

    </div>
</div>
