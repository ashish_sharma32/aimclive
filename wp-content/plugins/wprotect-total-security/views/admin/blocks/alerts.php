<?php
    if(!defined('_CHKINC')){
        die;
    }
?>
<div class="box-item">
    <div class="inside">
        <h2 class="box-title color-danger">
            <?php _e('Alerts', 'wprotect'); ?>
        </h2>

		<?php
			if(count($alerts)):
		?>
		<ol class="list-items">
		<?php

            foreach ($alerts as $key => $alert):

                $link_text = $key == 'exploitdb_detected' ? __('Check it', 'wprotect') : __('Fix it', 'wprotect');
        ?>
            <li>
                <?php echo $alert; ?> <a class="misc-info danger click-btn" href="<?php echo $fix_url[$key]; ?>"><?php echo $link_text; ?></a>            
            </li>
        <?php
            endforeach;
        ?>
		</ol>
		<?php
			else:
		?>
			<?php _e( 'Hey good news! Currently, there is no security alerts are detected!', 'wprotect' ); ?>
		<?php
			endif;
		?>

    </div>
</div>