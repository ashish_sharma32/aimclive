<?php

    if(!defined('_CHKINC')){
        die;
    }

    $create_nonce = wp_create_nonce( $ajax_nonce );

?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('Hide Wordpress', 'wprotect') ); ?>
    </h1>
     
    <?php echo $tab; ?>
    
    <div class="mainContainer hideadmin">
        
        <div class="grid-container">
            
            <div class="grid-6 center-block module-item grid-item">
                
                <div class="box-item">
                    <div class="inside">

                        <h2 class="box-title"><?php _e('Hide WP-Admin', 'wprotect'); ?> <a href="javascript:;" class="tooltip tooltip-anchor" title="<?php _e('Protection of WP-Admin restricts guests access to wp-admin directory. If you activate this protection, you will need to login wp-login.php page before accessing wp-admin directory.', 'wprotect'); ?>"><i class="dashicons dashicons-editor-help"></i></a></h2>
                        
                        <form id="wprotect-config" method="post" action="admin.php?page=<?php echo _WPROTECT_TEXTDOMAIN; ?>&tab=hide">
                            
                            <?php wp_nonce_field( $form_nonce, $textdomain.'_save_option_nonce' ); ?>

                            <?php
                                $key = 'hideadmin';
                                $status_hideadmin = $saved_options[$key] == 'on' ? true : false;
                            ?>
                            
                            <div class="list-item protection-status">

                                <span class="status-item label"><?php _e('Enable/Disable', 'wprotect'); ?>:</span>
                                <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                               
                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Hide way', 'wprotect'); ?>:</span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <?php
                                            foreach($hide_types as $type=>$name):     
                                               
                                        ?>
                                        <label><input <?php echo ganjargal_checkIt($type, $options_hideadmin['hide_type']); ?> type="radio" name="hide_type" value="<?php echo $type; ?>" /><?php echo $name; ?></label>
                                        <?php
                                            endforeach;
                                        ?>

                                    </div>

                                </div>

                            </div>
                            
                            <p align="center">
                                <button type="submit" class="form-submit"><i class="dashicons dashicons-yes"></i> <?php _e('Save settings', 'wprotect'); ?></button>
                            </p>

                        </form>

                    </div>

                </div>
                
                <div class="block-seperator"></div>

                <div class="box-item">
                    <div class="inside">

                        <h2 class="box-title"><?php _e('Hide Wp-login.php', 'wprotect'); ?> <a href="javascript:;" class="tooltip tooltip-anchor" title="<?php _e('The protection system hides login page against restricted guests and bots.', 'wprotect'); ?>"><i class="dashicons dashicons-editor-help"></i></a></h2>
                        
                        <form id="wprotect-config-hidelogin" method="post" action="admin.php?page=<?php echo _WPROTECT_TEXTDOMAIN; ?>&tab=hide">
                            
                            <?php wp_nonce_field( $form_nonce, $textdomain.'_save_option_nonce' ); ?>

                            <?php
                                $key = 'hidelogin';
                                $status_hidelogin = $saved_options[$key] == 'on' ? true : false;
                            ?>
                            
                            <div class="list-item protection-status">

                                <span class="status-item label"><?php _e('Enable/Disable', 'wprotect'); ?>:</span>
                                <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                               
                            </div>

                            <div class="form-helper">
                                <?php _e('Login URL', 'wprotect'); ?>: <?php echo home_url(); ?>/wp-login.php?<?php echo esc_html(@$options_hidelogin['key_name']); ?>=<?php echo esc_html(@$options_hidelogin['key_value']); ?>                                
                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Variable name', 'wprotect'); ?>:</span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" data-validetta="required,regExp[regname]" id="key_name" value="<?php echo esc_html(@$options_hidelogin['key_name']); ?>" class="form-input-text" name="key_name" />

                                    </div>

                                </div>

                            </div>

                            <div class="list-item">
                                
                                <div class="grid-container">
                                    
                                    <div class="grid-item grid-6">
                                        
                                        <span class="form-label"><?php _e('Variable value', 'wprotect'); ?>:</span>

                                    </div>

                                    <div class="grid-item grid-6">
                                        
                                        <input type="text" data-validetta="required" id="key_value" value="<?php echo esc_html(@$options_hidelogin['key_value']); ?>" class="form-input-text" name="key_value" />

                                    </div>

                                </div>

                            </div>
                            
                            <p align="center">
                                <button type="submit" class="form-submit"><i class="dashicons dashicons-yes"></i> <?php _e('Save settings', 'wprotect'); ?></button>
                            </p>

                        </form>

                    </div>

                </div>

                <div class="block-seperator"></div>

                <div class="box-item">
                    <div class="inside">
                        
                        <h2 class="box-title" id="hideversion"><?php _e('Hide Wordpress version', 'wprotect'); ?> <a href="javascript:;" class="tooltip tooltip-anchor" title="<?php _e('When the wordpress is not updated, the protection system hides your wordpress version number against hackers.', 'wprotect'); ?>"><i class="dashicons dashicons-editor-help"></i></a></h2>
                        
                        <?php
                            $key = 'hideversion';
                            $status_hideversion = $saved_options[$key] == 'on' ? true : false;
                        ?>

                        <div class="list-item protection-status">

                            <span class="status-item label"><?php _e('Enable/Disable', 'wprotect'); ?>:</span>
                            <span class="status-item action"><div class="toggle toggle-light" data-protection-type="<?php echo $key; ?>" id="toggle-<?php echo $key; ?>"></div></span>
                           
                        </div>

                    </div>
                </div>
                
            </div>                      
            
        </div>
         
    </div>
    
</div>
<script type="text/javascript">
                
    jQuery(function(){
        
        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

      jQuery('#toggle-hideadmin').toggles({
            on: <?php echo (int)$status_hideadmin; ?>,
            width: 60,
        });
        
        jQuery('#toggle-hideadmin').on('toggle', function(e, active) {
            
            var $this = jQuery(this);
            var $protection = $this.data('protection-type');
            var $action = active ? 'on' : 'off';
            
            var data = { 
                    'action': '<?php echo $ajax_method; ?>',
                    'protection': $protection,
                    'status': $action,
                    'nonce': '<?php echo $create_nonce; ?>'
            };
            
            jQuery.post(ajaxurl, data, function(responce){
                
                if(responce.result){
                    
                    if(responce.status == 'on'){
                        
                        toastr.success( responce.text );
                        
                    }else{
                        
                        toastr.warning( responce.text );
                        
                    }
                    
                }else{
                    
                    alert('<?php _e( 'Sorry! Try again!', 'wprotect'); ?>');
                    
                }
                
            }, 'json');
            
        });
        
        jQuery('#toggle-hidelogin').toggles({
            on: <?php echo (int)$status_hidelogin; ?>,
            width: 60,
        });
        
        jQuery('#toggle-hidelogin').on('toggle', function(e, active) {
            
            var $this = jQuery(this);
            var $protection = $this.data('protection-type');
            var $action = active ? 'on' : 'off';
            
            var data = { 
                    'action': '<?php echo $ajax_method; ?>',
                    'protection': $protection,
                    'status': $action,
                    'nonce': '<?php echo $create_nonce; ?>'
            };
            
            jQuery.post(ajaxurl, data, function(responce){
                
                if(responce.result){
                    
                    if(responce.status == 'on'){
                        
                        toastr.success( responce.text );
                        
                    }else{
                        
                        toastr.warning( responce.text );
                        
                    }
                    
                }else{
                    
                    alert('<?php _e( 'Sorry! Try again!', 'wprotect'); ?>');
                    
                }
                
            }, 'json');
            
        });

        jQuery('#toggle-hideversion').toggles({
            on: <?php echo (int)$status_hideversion; ?>,
            width: 60,
        });
        
        jQuery('#toggle-hideversion').on('toggle', function(e, active) {
            
            var $this = jQuery(this);
            var $protection = $this.data('protection-type');
            var $action = active ? 'on' : 'off';
            
            var data = { 
                    'action': '<?php echo $ajax_method; ?>',
                    'protection': $protection,
                    'status': $action,
                    'nonce': '<?php echo $create_nonce; ?>'
            };
            
            jQuery.post(ajaxurl, data, function(responce){
                
                if(responce.result){
                    
                    if(responce.status == 'on'){
                        
                        toastr.success( responce.text );
                        
                    }else{
                        
                        toastr.warning( responce.text );
                        
                    }
                    
                }else{
                    
                    alert('<?php _e( 'Sorry! Try again!', 'wprotect'); ?>');
                    
                }
                
            }, 'json');
            
        });

        jQuery("#wprotect-config-hidelogin").validetta({
            bubblePosition: 'bottom',
            realTime : true,
            validators: {
                regExp: {
                    regname : {
                        pattern : /^[a-zA-Z0-9_]+$/,
                        errorMessage : 'Only letters & underline(_) allowed!'
                    }
                }
            }
        });

        jQuery('.tooltip').tooltipster({
            theme: 'tooltipster-borderless',
            maxWidth: 200,
            side: 'right'
        });

    });
    
</script>