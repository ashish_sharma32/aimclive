<?php
    if(!defined('_CHKINC')){
        die;
    }
?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('Security alerts', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer alerts">
        
        <div class="grid-container">
            
            <div class="grid-6 center-block module-item grid-item">
                
                <div class="box-item">
                    <div class="inside">
                        <?php
                            if(count($alerts)):
                        ?>

                        <h2 class="box-title color-danger">
                            <?php _e('Please fix the following problem:', 'wprotect'); ?>
                        </h2>

                        <ol class="list-items">
                        <?php

                            foreach ($alerts as $key => $alert):

                                $link_text = $key == 'exploitdb_detected' ? __('Check it', 'wprotect') : __('Fix it', 'wprotect');
                        ?>
                            <li>
                                <?php echo $alert; ?> <a class="misc-info danger click-btn" href="<?php echo $fix_url[$key]; ?>"><?php echo $link_text; ?></a>            
                            </li>
                        <?php
                            endforeach;
                        ?>
                        </ol>
                        <?php
                            else:
                        ?>
                            <?php _e( 'Hey good news! Currently, there is no security alerts are detected!', 'wprotect'); ?>
                        <?php
                            endif;
                        ?>
                    </div>
                </div>
                
            </div>                      
            
        </div>
         
    </div>
    
</div>
