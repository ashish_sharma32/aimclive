<?php
    if(!defined('_CHKINC')){
        die;
    }
?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('WProtect', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer main">
       
        <div class="grid-container">
                        
            <div class="grid-item grid-4 home-block">

                <?php echo $statuses; ?>

            </div>

            <div class="grid-item grid-4 home-block">
                
                <?php echo $alerts; ?>

            </div>

            <div class="grid-item grid-4 home-block">

                <?php echo $logs; ?>

            </div>

        </div>
         
    </div>
    
</div>
