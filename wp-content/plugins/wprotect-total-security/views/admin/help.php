<?php

    if(!defined('_CHKINC')){
        die;
    }

?>
<div class="wrap <?php echo _WPROTECT_TEXTDOMAIN; ?>">
    
    <h1 class="module-title">
        <?php echo ganjargal_getLogo( $logo, __('WProtect - Help', 'wprotect') ); ?>
    </h1>
    
    <?php echo $tab; ?>
    
    <div class="mainContainer help">
        
        <div class="grid-container">
            
            <div class="grid-6 center-block module-item grid-item">
               
                <div class="box-item">
                    <div class="inside">
                        
                        <p align="center">Email: <a href="mailto:wprotect-support@ganjargal.pro">wprotect-support@ganjargal.pro</a></p>

                        <h2 align="center">
                            <a href="http://www.ganjargal.pro/wpdemo/videos" class="misc-info info click-btn btn-med" target="_blank"><i class="dashicons dashicons-video-alt3"></i> <?php _e('Watch video tutorials', 'wprotect'); ?></a>
                        

                    </div>
                </div>
                
            </div>                      
            
        </div>
         
    </div>
    
</div>
