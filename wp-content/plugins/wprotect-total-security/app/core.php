<?php

if(!defined('_CHKINC')){
    die;
}

class WProtectAPI
{

	function __construct(){}

	protected function getLogs($limit=0)
	{
		global $wpdb;

		$tablename = $wpdb->prefix._WPROTECT_TEXTDOMAIN.'_logs';
		$users_tablename = $wpdb->prefix.'users';

		$query = "SELECT COUNT(1) FROM ".$tablename." AS combined_table";
     	$total = $wpdb->get_var( $query );

     	$items_per_page = $limit > 0 ? $limit : 20;

     	$page = isset( $_GET['paged'] ) ? abs( (int) $_GET['paged'] ) : 1;

     	$offset = ( $page * $items_per_page ) - $items_per_page;

     	$rows =  $wpdb->get_results("
     			SELECT l.*, u.user_login FROM ".$tablename." l 
     			LEFT JOIN ".$users_tablename." AS u ON u.ID = l.user_id 
     			ORDER BY l.created_at DESC LIMIT ".$offset.", ".$items_per_page);

     	$pagination = paginate_links( array(
					        'base' => add_query_arg( 'paged', '%#%' ),
					        'format' => '',
					        'prev_text' => __('&laquo;'),
					        'next_text' => __('&raquo;'),
					        'total' => ceil($total / $items_per_page),
					        'current' => $page
					    ));

     	return array( 'rows' => $rows, 'pagination' => $pagination, 'offset' => $offset );

	}

	protected function saveOption($tab = null)
	{
		global $wpdb;

		if(!is_null($tab)){

			$extra_options = ganjargal_getPluginOption('extra_options');
			$options = array();
			$field = null;

			switch($tab){

				case 'hide':

				if(class_exists('WProtect_HideAdmin'))
				{
				
					if(isset($_POST['hide_type'])){

	                    $hideadmin = new WProtect_HideAdmin;
						$hide_type = esc_sql(esc_html($_POST['hide_type']));
	                    
	                    if($hideadmin->checkType($hide_type)){
	                        
	                        $options['hide_type'] = $hide_type;
	                        $field = WProtect_HideAdmin::getOptionFieldName();
	                        
	                    }

	                }

				}
				
				if(class_exists('WProtect_HideLogin'))
				{

					if(isset($_POST['key_name']) && isset($_POST['key_value']) && preg_match('/^[a-zA-Z0-9_]+$/', $_POST['key_name'])){

						$key_name = esc_sql(esc_html(trim($_POST['key_name'])));
						$key_value = esc_sql(esc_html(trim($_POST['key_value'])));

						$options['key_name'] = $key_name;
						$options['key_value'] = $key_value;
	                    $field = WProtect_HideLogin::getOptionFieldName();

					}

				}

				break;

				case 'exploitdb':

				if(class_exists('WProtect_ExploitDB'))
				{
                    
					$auto_disable_plugin = esc_sql(esc_html($_POST['auto_disable_plugin']));
					$scheduled_check = esc_sql(esc_html($_POST['scheduled_check']));
                    
                    if( check_onNoff($auto_disable_plugin) &&  
                    	check_onNoff($scheduled_check)){
                        
                        $options['auto_disable_plugin'] = $auto_disable_plugin;
                    	$options['scheduled_check'] = $scheduled_check;
                        $field = WProtect_ExploitDB::getOptionFieldName();
                        
                    }

				}	

				break;

				case 'bruteforce':

				if( isset($_POST['username_admin']) &&
					$_POST['username_admin'] !== '' && 
					validate_username($_POST['username_admin']) ){

					$username = esc_html($_POST['username_admin']);

                    if( !username_exists($username) ){

                    	$wpdb->update(
			                $wpdb->prefix.'users',
			                array(
			                        'user_login' => $username
			                ),
			                array( 'user_login' => 'admin' ),
			                array(
			                        '%s'
			                )
			            );

			            unset($extra_options['alerts']['bruteforce']);

			            $user_id = ganjargal_getLoggedUserID();

			            wprotect_log( 'system', 'on', sprintf(__( 'Username is changed by %s', 'wprotect'), $username), $user_id );

                    }

				}

				if(class_exists('WProtect_Bruteforce'))
				{

					if(	intval($_POST['attempts_limit']) > 0 ){

						$options['attempts_limit'] = intval($_POST['attempts_limit']);
						
						$field = WProtect_Bruteforce::getOptionFieldName();

						$extra_options['recaptcha_site_key'] = esc_sql(esc_html(trim($_POST['recaptcha_site_key'])));
						$extra_options['recaptcha_secret_key'] = esc_sql(esc_html(trim($_POST['recaptcha_secret_key'])));
						
						if(	$extra_options['recaptcha_site_key'] !== '' && 
							$extra_options['recaptcha_secret_key'] !== ''){

							unset($extra_options['alerts']['recaptcha']);

						}

					}

				}

				ganjargal_updatePluginOption('extra_options', $extra_options);

				break;

				case 'inputfilter':

				if(class_exists('WProtect_InputFilter'))
				{

					$options['enable_xss'] = isset($_POST['enable_xss']) ? 'on' : 'off';
					$options['filter_get'] = isset($_POST['filter_get']) ? 'on' : 'off';
					$options['filter_post'] = isset($_POST['filter_post']) ? 'on' : 'off';
					$options['filter_cookies'] = isset($_POST['filter_cookies']) ? 'on' : 'off';
					$options['filter_file_inclusion'] = isset($_POST['filter_file_inclusion']) ? 'on' : 'off';

					$field = WProtect_InputFilter::getOptionFieldName();

				}	

				break;

				case 'settings':

					$themes = $this->admin_themes;
					$theme = esc_sql(esc_html(trim($_POST['admin_theme'])));

					if(in_array($theme, $themes)){
						$extra_options['theme_style'] = $theme;
					}

					$alert_email = esc_sql(esc_html(trim($_POST['alert_email'])));

					if(is_email($alert_email)){
						$extra_options['alert_email'] = $alert_email;
					}

					$onesignal_app_id = esc_sql(esc_html(trim($_POST['onesignal_app_id'])));
					$onesignal_api_key = esc_sql(esc_html(trim($_POST['onesignal_api_key'])));
					$onesignal_subdomain = esc_sql(esc_html(trim($_POST['onesignal_subdomain'])));
					$onesignal_safari_webid = esc_sql(esc_html(trim($_POST['onesignal_safari_webid'])));

					if($onesignal_app_id !== ''){
						$extra_options['onesignal_app_id'] = $onesignal_app_id;
					}

					if($onesignal_api_key !== ''){
						$extra_options['onesignal_api_key'] = $onesignal_api_key;
					}

					if($onesignal_subdomain !== ''){
						$extra_options['onesignal_subdomain'] = $onesignal_subdomain;
					}

					if($onesignal_safari_webid !== ''){
						$extra_options['onesignal_safari_webid'] = $onesignal_safari_webid;
					}

					if(	$extra_options['onesignal_app_id'] !== '' && 
						$extra_options['onesignal_api_key'] !== '' &&
						$extra_options['onesignal_subdomain'] !== '' &&
						$extra_options['onesignal_safari_webid'] !== '' ){

						unset($extra_options['alerts']['onesignal']);

					}

					ganjargal_updatePluginOption('extra_options', $extra_options);

				break;

				case 'ban':

				if(class_exists('WProtect_BanIP'))
				{

					if(isset($_POST['ban_time'])){

						$ban_time = intval($_POST['ban_time']);

						if($ban_time > 0){

							$options['ban_time'] = $ban_time;
							$field = WProtect_BanIP::getOptionFieldName();

						}

					}

					if(isset($_POST['add_ip'])){

						$add_ip = esc_html($_POST['add_ip']);

						$banip = new WProtect_BanIP;
						$ip = $banip->addBanIp($add_ip);

						if($ip){

							$user_id = ganjargal_getLoggedUserID();

							wprotect_log( 'core', 'bannedip', sprintf( __( '%s ip address is banned!', 'wprotect'), $ip ), $user_id );

						}

					}

				}

				break;

			}

			if( count($options) > 0 && 
				!is_null($field) ){
				
				ganjargal_updatePluginOption($field, $options);

			}

		}

		return;

	}

	protected function setProtectionsStatuses($protection_types=array())
	{

		$return = array();

		$status = ganjargal_getPluginOption('protection_statuses');

		$count = count($status);

		if(is_array($status) && $count > 0){

			foreach( $protection_types as $key => $val){

				if(!array_key_exists($key, $status)){

					$status[$key] = $val['default_value'];

				}

			}

			if($count < count($status)){

				ganjargal_updatePluginOption('protection_statuses', $status);

			}			

			$return = $status;

		}

		return $return;

	}

}