<?php

if(!defined('_CHKINC')){
    die;
}

if( !class_exists('WProtectRecaptcha')):

class WProtectRecaptcha{

	function __construct()
	{}

	public static function verifyCaptcha($secretKey = '', $user = array())
	{

    if($secretKey == ''){
      
      return $user[0];

    }

		if( !isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])){

            return new WP_Error( 'empty_captcha', __('CAPTCHA should not be empty!', 'wprotect'));
          
        }else{

            $url = 'https://www.google.com/recaptcha/api/siteverify';
            
            $options = array();

            $options['response'] = esc_attr($_POST['g-recaptcha-response']);
            $options['ip'] = ganjargal_getClientIP();
            $options['secret'] = $secretKey;

            $request = wp_remote_post( $url, array('body' => $options) );
            $response_body = wp_remote_retrieve_body( $request );
            $answers = explode( "\n", $response_body );
            $request_status = trim( $answers[0] );
            
            if($request_status == false){

                return new WP_Error( 'invalid_captcha', __('CAPTCHA response was incorrect!', 'wprotect'));

            }

        }

    return $user[0];

	}

	public static function attachHead()
	{
?>
	<script src='https://www.google.com/recaptcha/api.js'></script>
<?php
	}

	public static function drawCaptcha($siteKey = '')
	{

		self::htmlCaptcha( $siteKey );

	}

	public static function htmlCaptcha( $siteKey = '' )
	{

?>

	<div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
    <noscript>
      <div>
        <div style="width: 302px; height: 422px; position: relative;">
          <div style="width: 302px; height: 422px; position: absolute;">
            <iframe src="https://www.google.com/recaptcha/api/fallback?k=<?php echo $siteKey; ?>"
                    frameborder="0" scrolling="no"
                    style="width: 302px; height:422px; border-style: none;">
            </iframe>
          </div>
        </div>
        <div style="width: 300px; height: 60px; border-style: none;
                       bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
                       background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
          <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                       class="g-recaptcha-response"
                       style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
                              margin: 10px 25px; padding: 0px; resize: none;" >
          </textarea>
        </div>
      </div>
    </noscript>

<?php

	}

}

endif;