<?php

if(!defined('_CHKINC')){
    die;
}

if(!function_exists('__loadWProtect')){

    function __loadWProtect()
    {    

        $object = new WProtect;
        return $object;    
    }

}

if(!function_exists('WProtectLoadProtections')){

    function WProtectLoadProtections(){

        //protections
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.banip.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.bruteforce.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.inputfilter.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.exploitdb.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.hideadmin.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.hidelogin.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.directories.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.disallowfileedit.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'protections'.DIRECTORY_SEPARATOR.'protection.hideversion.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'core.php' );
        require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'admin.php' );

    }

}

if ( !function_exists('wprotect_log')){

	function wprotect_log( $log_type = null, $log_action = null, $log_msg = null, $user_id = 0 )
	{
		global $wpdb;

		$tablename = $wpdb->prefix._WPROTECT_TEXTDOMAIN.'_logs';

		$wpdb->insert(
            $tablename,
            array(
                'user_id' => $user_id,
            	'log_type' => $log_type,
            	'log_action' => $log_action,
            	'log_message' => $log_msg,
            	'created_at' => time()
            ),
            array(
                    '%d',
                    '%s',
                    '%s',
                    '%s',
                    '%d'
            )
        );
	}

}

if( !function_exists('ganjargal_getLoggedUserID') )
{

    function ganjargal_getLoggedUserID()
    {
        
       return (int)get_current_user_id();
      
    }

}

if( !function_exists('ganjargal_getClientIP') )
{

    function ganjargal_getClientIP()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

}

if( !function_exists('ganjargal_getPluginOption') )
{

    function ganjargal_getPluginOption( $option_name = '', $default_value = false )
    {
        return get_option( _WPROTECT_TEXTDOMAIN.'_'.$option_name, $default_value );
    }

}

if( !function_exists('ganjargal_updatePluginOption') )
{

    function ganjargal_updatePluginOption( $option_name = '', $default_value = false )
    {
        return update_option( _WPROTECT_TEXTDOMAIN.'_'.$option_name, $default_value );
    }

}

if( !function_exists('ganjargal_logActionStyle') )
{

    function ganjargal_logActionStyle( $log_type = '', $log_action = '' )
    {
     	
    	$return = array();

    	switch($log_action){

    		default:
                $return['icon'] = 'dashicons-warning';
                $return['style'] = 'info';
            break;
    		case 'off':
            case 'stop':
            case 'done':
    		case 'disabled':
    			$return['icon'] = 'dashicons-no';
    			$return['style'] = 'warning';
    		break;

    		case 'on':
            case 'start':
    		case 'enabled':
    			$return['icon'] = 'dashicons-yes';
    			$return['style'] = 'success';
    		break;

    	}

    	return $return;

    }

}

if( !function_exists('ganjargal_sentMail') )
{
    function ganjargal_sentMail($content_type)
    {
        return 'text/html';
    }
}

add_filter('wp_mail_content_type','ganjargal_mailHTML_type');

if( !function_exists('ganjargal_sentMail') )
{

    function ganjargal_sentMail( $subject = '', $message = '' )
    {
        
        $extra_options = ganjargal_getPluginOption( 'extra_options' );

        $to = $extra_options['alert_email'];
        $users = get_users('role=administrator'); 

        if($to == '' && count($users)){

            $to = $users[0]->user_email;

        }

        $_subject = 'WProtect: ' . $subject;

        $_message = 'URL: '.home_url().'<br/>';
        $_message .= 'Alert message: </br>';
        $_message .= $message;

        @wp_mail($to, $_subject, $_message);

    }

}

if( !function_exists('ganjargal_checkIt') )
{

    function ganjargal_checkIt( $value = '', $checked_value = 'on' )
    {
        
        if($value == $checked_value){

            return 'checked="checked"';

        }

    }

}

if ( !function_exists('check_onNoff'))
{
    function check_onNoff($value = 'on')
    {
       return in_array($value, array('on', 'off'));
    }
}

if ( !function_exists('ganjargal_getLogo'))
{
    function ganjargal_getLogo($logo = array(), $title = '')
    {
       
        return '<a href="'.$logo['link'].'" class="logo-wrapper"><img src="'.$logo['img'].'" />'.$title.'</a>';

    }
}

if( !function_exists('ganjargal_curlSupport') )
{

    function ganjargal_curlSupport()
    {

        return in_array('curl', get_loaded_extensions());

    }

}

if ( !function_exists('ganjargal_pushNotification'))
{
    function ganjargal_pushNotification($title, $message, $url = null)
    {   

        $extra_options = ganjargal_getPluginOption('extra_options');

        if( $extra_options['onesignal_app_id'] !== '' && 
            $extra_options['onesignal_api_key'] !== '' ){
       
            if(ganjargal_curlSupport()){

                $fields = array(
                    'app_id' => $extra_options['onesignal_app_id'],
                    'contents' => array(
                        'en' => $message,
                    ),
                    'url' => $url,
                    'included_segments' => 'All'
                );

                $fields = json_encode($fields);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                    'Authorization: Basic '.$extra_options['onesignal_api_key']));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($ch);
                curl_close($ch);
                
                return $response;

            }

        }

    }
}

if ( !function_exists('remove_invisible_characters'))
{
	function remove_invisible_characters($str, $url_encoded = TRUE)
	{
		$non_displayables = array();
		
		if ($url_encoded)
		{
			$non_displayables[] = '/%0[0-8bcef]/';	// url encoded 00-08, 11, 12, 14, 15
			$non_displayables[] = '/%1[0-9a-f]/';	// url encoded 16-31
		}
		
		$non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';	// 00-08, 11, 12, 14-31, 127

		do
		{
			$str = preg_replace($non_displayables, '', $str, -1, $count);
		}
		while ($count);

		return $str;
	}
}

if ( !function_exists('ganjargal_is_php'))
{
	function ganjargal_is_php($version = '5.0.0')
	{
		static $_is_php;
		$version = (string)$version;

		if ( ! isset($_is_php[$version]))
		{
			$_is_php[$version] = (version_compare(PHP_VERSION, $version) < 0) ? FALSE : TRUE;
		}

		return $_is_php[$version];
	}
}