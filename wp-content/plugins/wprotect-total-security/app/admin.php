<?php

if(!defined('_CHKINC')){
    die;
}

if(!class_exists('WProtectAdmin')){

class WProtectAdmin extends WProtectAPI
{

    static protected $version = '1.0.0';
    protected $capability = 'manage_options';
    protected $statuses = array();
    private $active_tab = 'main';
    private $admin_nonces = array();
    private $admin_menus = array(
                                'parent'    =>      array(
                                                        'page_title'    =>  'WProtect - Total security',
                                                        'menu_title'    =>  'WProtect',
                                                        'capability'    =>  '',
                                                        'menu_slug'     =>  'wprotect',
                                                        'method'        =>  'mainView',
                                                        'icon'          =>  'dashicons-shield-alt',
                                                        'position'      =>  '60.2'
                                                    ),

                                'sub'       =>     array(
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Protections status - WProtect',
                                                                    'menu_title'    =>  'Main',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'main',
                                                                    'method'        =>  'mainView'
                                                            )
                                                            ,
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Security alerts - WProtect',
                                                                    'menu_title'    =>  'Alerts',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'alerts',
                                                                    'method'        =>  'mainView'
                                                            ),        
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Exploit DB',
                                                                    'menu_title'    =>  'Exploit DB',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'exploitdb',
                                                                    'method'        =>  'mainView'
                                                            ),
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Hide WP-Admin - WProtect',
                                                                    'menu_title'    =>  'Hide',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'hide',
                                                                    'method'        =>  'mainView'
                                                            ),
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Brute-force - WProtect',
                                                                    'menu_title'    =>  'Blocking Brute-force',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'bruteforce',
                                                                    'method'        =>  'mainView'
                                                            ),
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Input filter - WProtect',
                                                                    'menu_title'    =>  'Input Filter',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'inputfilter',
                                                                    'method'        =>  'mainView'
                                                            ),
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Other protections',
                                                                    'menu_title'    =>  'Other protections',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'other',
                                                                    'method'        =>  'mainView'
                                                            ),
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'IP Ban',
                                                                    'menu_title'    =>  'IP Ban',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'ban',
                                                                    'method'        =>  'mainView'
                                                            ),
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Logs',
                                                                    'menu_title'    =>  'Logs',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'logs',
                                                                    'method'        =>  'mainView'
                                                            ),        
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Settings - WProtect',
                                                                    'menu_title'    =>  'Settings',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'settings',
                                                                    'method'        =>  'mainView'
                                                            ),
                                                            array(
                                                                    'parent_slug'   =>  '',
                                                                    'page_title'    =>  'Help',
                                                                    'menu_title'    =>  '<i class="dashicons dashicons-editor-help"></i> Help',
                                                                    'capability'    =>  '',
                                                                    'menu_slug'     =>  'wprotect',
                                                                    'tabid'         =>  'help',
                                                                    'method'        =>  'mainView'
                                                            )
                                                    )

                        );

    static protected $options = array(
    'protect_types' => array(
                         'bruteforce'  =>  array(
                                                    'title' => 'Blocking Brute-force',
                                                    'default_value' => 'on',
                                                    'is_live' => true,
                                                    'autoload' => 'yes',
                                                    'deprecated' => '',
                                                    'init_method' => array( 'WProtect_Bruteforce', 'init' )

                                                 ),

                          'hideadmin'  =>  array(
                                                    'title' => 'Hide WP-Admin',
                                                    'default_value' => 'off',
                                                    'is_live' => true,
                                                    'autoload' => 'yes',
                                                    'deprecated' => '',
                                                    'init_method' => array( 'WProtect_HideAdmin', 'init' )

                                                 ),
                          'hidelogin'  =>  array(
                                'title' => 'Hide Login',
                                'default_value' => 'off',
                                'is_live' => true,
                                'autoload' => 'yes',
                                'deprecated' => '',
                                'init_method' => array( 'WProtect_HideLogin', 'init' )

                          ),


                          'inputfilter'  =>  array(
                                                    'title' => 'Input filter',
                                                    'default_value' => 'on',
                                                    'is_live' => true,
                                                    'autoload' => 'yes', 
                                                    'deprecated' => '',
                                                    'init_method' => array( 'WProtect_InputFilter', 'init' )

                                                 ),
                          'exploitdb'   =>  array(
                                                    'title' => 'Exploit database',
                                                    'default_value' => 'on',
                                                    'is_live' => true,
                                                    'autoload' => 'yes',
                                                    'deprecated' => '',
                                                    'init_method' => array( 'WProtect_ExploitDB', 'init' )
                                                ),
                          'banip'   =>  array(
                                                    'title' => 'Ban IP',
                                                    'default_value' => 'on',
                                                    'is_live' => true,
                                                    'autoload' => 'yes',
                                                    'deprecated' => '',
                                                    'init_method' => array( 'WProtect_BanIP', 'init' )
                                                ),
                          'disallowfileedit'   =>  array(
                                                    'title' => 'Disallow file edit',
                                                    'default_value' => 'on',
                                                    'is_live' => true,
                                                    'autoload' => 'yes',
                                                    'deprecated' => '',
                                                    'init_method' => array( 'WProtect_DisallowFileEdit', 'init' )
                                                ),
                          'hideversion'   =>  array(
                                                    'title' => 'Hide Wordpress version',
                                                    'default_value' => 'on',
                                                    'is_live' => true,
                                                    'autoload' => 'yes',
                                                    'deprecated' => '',
                                                    'init_method' => array( 'WProtect_HideVersion', 'init' )
                                                )
                      ),
'extra_options' => array(

                            'required_php_configs' => array(
                                                    'title' => 'Change some PHP values of configuration for security',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                            ),

                            'alert_email'   => array(
                                                    'title' => 'Sent alert notification email',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                            ),

                            'theme_style'    =>  array(
                                                    'title' => 'Theme style',
                                                    'default_value' => 'dark',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                                                 ),
                            'recaptcha_site_key'     =>  array(
                                                    'title' => 'reCaptcha site key',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                                                 ),
                            'recaptcha_secret_key'     =>  array(
                                                    'title' => 'reCaptcha secret key',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                                                 ),
                            'onesignal_app_id'     =>  array(
                                                    'title' => 'OneSignal App ID',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                                                 ),
                            'onesignal_api_key'     =>  array(
                                                    'title' => 'OneSignal API key',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                                                 ),
                            'onesignal_subdomain'     =>  array(
                                                    'title' => 'OneSignal subdomain',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                                                 ),
                            'onesignal_safari_webid'     =>  array(
                                                    'title' => 'OneSignal Web ID',
                                                    'default_value' => '',
                                                    'autoload' => 'yes',
                                                    'deprecated' => ''
                                                 ),
                            'alerts'    => array(
                                                'title' => 'Alerts',
                                                'default_value' => array(),
                                                'autoload' => 'yes',
                                                'deprecated' => ''   
                                )

                    )

                            );

    private $ajax_methods = array(
                                    'protection_status',
                                    'check_username',
                                    'other_protections',
                                    'remove_ban',
                                    'exdb_check'
                            );

    static protected $db_tables = array(
                                    'log'   =>  'logs'
                            );

    protected $alert_fix_urls = array();

    protected $admin_themes = array( 'dark', 'light' );

    private $required_php_configs = array(
                                            'allow_url_include' => 'Off',
                                            'register_globals' => 'Off',
                                            'magic_quotes_gpc' => 'Off',
                                            'session.auto_start' => 'Off',
                                            'expose_php' => 'Off',
                                            'ignore_repeated_errors' => 'Off',
                                            'disable_functions' => 'exec,passthru,shell_exec,system,proc_open,popen,parse_ini_file,show_source'

                                        );

    function __construct()
    {

        $this->setPHPini();

        $this->statuses = $this->setProtectionsStatuses(self::$options['protect_types']);

        if(is_admin()){

            add_action( 'admin_menu', array( $this, 'adminCreateMenu' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'loadJcss') );

            add_filter( 'admin_body_class', array( $this, 'adminBodyClass') );

            $this->admin_nonces['protection_status'] = _WPROTECT_TEXTDOMAIN .'-from-protection-status';
            $this->admin_nonces['check_username'] = _WPROTECT_TEXTDOMAIN .'-from-check-username';
            $this->admin_nonces['save_option'] = _WPROTECT_TEXTDOMAIN .'-save-option';
            $this->admin_nonces['other_protections'] = _WPROTECT_TEXTDOMAIN .'-other-protection';
            $this->admin_nonces['settings'] = _WPROTECT_TEXTDOMAIN .'-from-settings';
            $this->admin_nonces['remove_ban'] = _WPROTECT_TEXTDOMAIN .'-from-remove-ban';
            $this->admin_nonces['exdb_check'] = _WPROTECT_TEXTDOMAIN .'-from-exdb-check';
            
            $this->alert_fix_urls['bruteforce'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=bruteforce';
            $this->alert_fix_urls['recaptcha'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=bruteforce#recaptcha';
            $this->alert_fix_urls['onesignal'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=settings#onesignal';
            $this->alert_fix_urls['exploitdb'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=exploitdb';
            $this->alert_fix_urls['exploitdb_detected'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=exploitdb&autoscan=true#scanner-wrapper';
            $this->alert_fix_urls['upload_phpkill'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=other#upload_phpkill';
            $this->alert_fix_urls['directory_listing'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=other#directory_listing';
            $this->alert_fix_urls['robots'] = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=other#robots';

            foreach( $this->ajax_methods as $method ){

                $action_name = 'wp_ajax_'._WPROTECT_TEXTDOMAIN.'_'.$method;
                $method_name = 'ajax_'.$method;

                add_action( $action_name, array( $this, $method_name ) );

            }

            $this->active_tab = preg_replace('/^[a-z0-9]$/', '', @$_GET['tab']);

            add_action('admin_head', array($this, 'adminHead'));
            add_action('admin_footer', array($this, 'adminFooter'));    

        }

        if(function_exists('session_start')){

            if(!session_id()) {
                session_start();
            }

            add_action('wp_logout', array($this, 'endSession'));    

        }        

    }

    protected function setPHPini()
    {

        foreach ($this->required_php_configs as $key => $value) {
           
            @ini_set($key, $value);

        }

    }

    public function endSession()
    {
        @session_destroy();
    }

    static public function init()
    {

        register_activation_hook( _PLUGIN_FILE_WPROTECT, array( 'WProtect', 'activatePlugin') );
        register_deactivation_hook( _PLUGIN_FILE_WPROTECT, array( 'WProtect', 'deactivatePlugin') );
        add_action( 'init', '__loadWProtect', 0 );
    }

    static public function activatePlugin()
    {

        self::createDBtable( 'log' );
        
        self::setDefaultOptions();

        $user_id = ganjargal_getLoggedUserID();

        $protect_type_statutes = array();

        foreach( self::$options['protect_types'] as $key => $val){

            $protect_type_statutes[$key] = $val['default_value'];

            if( is_array($val['init_method']) && 
                class_exists($val['init_method'][0]) ){
                
                $class = $val['init_method'][0];                
                $object = new $class();
                $method = $val['init_method'][1];
                $object->$method();

                if($val['default_value'] == 'on'){

                    wprotect_log( 'protection-status', 'on', sprintf( __( '%s protection is turned on', 'wprotect'), ucfirst($val['title']) ), $user_id );

                }

            }

        }

        add_option( _WPROTECT_TEXTDOMAIN.'_protection_statuses', $protect_type_statutes, '', 'yes' );
        add_option( _WPROTECT_TEXTDOMAIN.'_version', self::$version, '', 'yes' );
        
        $self = new self();        
        $self->checkAlerts();

        wprotect_log( 'core', 'enabled', __('WProtect is enabled.', 'wprotect'), $user_id );

    }

    static private function setDefaultOptions()
    {

        $extra_options = array();

        foreach( self::$options['extra_options'] as $key => $val){

            $extra_options[$key] = $val['default_value'];

        }

        add_option( _WPROTECT_TEXTDOMAIN.'_extra_options', $extra_options, '', 'yes' );

    }

    static public function deactivatePlugin()
    {

        delete_option( _WPROTECT_TEXTDOMAIN.'_protection_statuses' );

        $user_id = ganjargal_getLoggedUserID();

        wprotect_log( 'core', 'disabled', __('WProtect is disabled!', 'wprotect'), $user_id );

    }

    protected function liveProtecting()
    {

        if(is_array($this->statuses) && count($this->statuses) > 0){

            foreach ($this->statuses as $key=>$status) {
                
                $protection = self::$options['protect_types'][$key];

                if( $protection['is_live'] == true && 
                    $status == 'on'                    
                   ){

                    if( is_array($protection['init_method']) && 
                        class_exists($protection['init_method'][0]) ){

                        $class = $protection['init_method'][0];
                        $object = new $class();
                        $object->registerActions();

                    }                   

                }

            }

        }

        if ( !wp_next_scheduled( _WPROTECT_TEXTDOMAIN.'_check_alerts' ) ) {
            
          wp_schedule_event( time(), 'daily', _WPROTECT_TEXTDOMAIN.'_check_alerts' );

        }

        add_action( _WPROTECT_TEXTDOMAIN.'_check_alerts', array( $this, 'checkAlerts' )) ;

        if ( !wp_next_scheduled( _WPROTECT_TEXTDOMAIN.'_sent_alerts' ) ) {
            
          wp_schedule_event( time(), 'daily', _WPROTECT_TEXTDOMAIN.'_sent_alerts' );

        }

        add_action( _WPROTECT_TEXTDOMAIN.'_sent_alerts', array( $this, 'sentAlerts' )) ;
        
        $this->getAlert('makeNotice');

    }


    static private function createDBtable( $type )
    {   
        global $wpdb;
        
        if( !function_exists('dbDelta')){

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        }

        $tablename = $wpdb->prefix._WPROTECT_TEXTDOMAIN.'_'.self::$db_tables[$type];

        $query = '';

        switch($type){

            case 'log':

                $query = "CREATE TABLE IF NOT EXISTS `".$tablename."` (
                          `ID` bigint(20) NOT NULL AUTO_INCREMENT,
                          `user_id` int(11) DEFAULT '0',
                          `log_type` varchar(20) DEFAULT NULL,
                          `log_action` varchar(20) DEFAULT NULL,
                          `log_message` mediumtext,
                          `created_at` int(11) DEFAULT NULL,
                          PRIMARY KEY (`ID`),
                          KEY `uidx` (`user_id`)
                        );";

            break;

        }
        
        if($query !== ''){

            dbDelta( $query );

        }

    }

    private function validNonce( $nonce_name, $action )
    {

        $action = $this->admin_nonces[$action];

        if ( !isset( $_POST[$nonce_name] ) ) {
            return false;
        }

        $field  = wp_unslash( $_POST[$nonce_name] );

        return wp_verify_nonce( $field, $action );

    }

    public function checkAlerts( $return = null )
    {
        global $wpdb;

        $extra_options = ganjargal_getPluginOption('extra_options');

        if(!isset($extra_options['theme_style'])){
            
            self::setDefaultOptions();
            $extra_options = ganjargal_getPluginOption('extra_options');

        }

        $alerts = array();

        include_once( ABSPATH .'wp-includes/pluggable.php' );

        if( username_exists('admin') ){

            $alerts['bruteforce'] = __( 'Change the Default “admin” username!', 'wprotect' );

        }

        if( $this->statuses['bruteforce'] == 'on' && 
            class_exists('WProtectRecaptcha') && 
            class_exists('WProtect_Bruteforce')){

            if( !isset($extra_options['recaptcha_site_key']) ||  
                !isset($extra_options['recaptcha_secret_key']) ||
                $extra_options['recaptcha_site_key'] == '' || 
                $extra_options['recaptcha_secret_key'] == ''
            ){

                $alerts['recaptcha'] = __( 'RECaptcha keys are empty!', 'wprotect' );

            }

        }

        if( !isset($extra_options['onesignal_app_id']) ||  
            !isset($extra_options['onesignal_api_key']) ||
            !isset($extra_options['onesignal_subdomain']) ||
            $extra_options['onesignal_app_id'] == '' || 
            $extra_options['onesignal_api_key'] == '' || 
            $extra_options['onesignal_subdomain'] == ''
        ){

            $alerts['onesignal'] = __( 'OneSignal keys are empty!', 'wprotect' );

        }

        if($extra_options['alert_email'] == ''){

            $users = get_users('role=administrator'); 

            if(count($users)){

                $extra_options['alert_email'] = $users[0]->user_email;

            }

        }      

        if( $this->statuses['exploitdb'] == 'on' && 
            class_exists('WProtect_ExploitDB')){

            $exploitdb = new WProtect_ExploitDB;

            if(!$exploitdb->checkDBfile()){
                $alerts['exploitdb'] = sprintf( __( 'Exploit database is not updated! <a href="%s">Click here</a> to update!', 'wprotect'), 'admin.php?page=wprotect&tab=exploitdb' );
            }

        }

        if(class_exists('WProtect_ProtectDirectories'))
        {

            $protection_directories = new WProtect_ProtectDirectories;

            if(!$protection_directories->checkUploadDirectory()){

                $alerts['upload_phpkill'] = __( 'PHP execution in "wp-content/uploads" directory is not prevented!', 'wprotect' );
                
            }

            if(!$protection_directories->checkRootHtaccess()){

                $alerts['directory_listing'] = __( '"Directory listing" is not disabled!', 'wprotect' );

            }
            
            if(!$protection_directories->checkRobots()){

                $alerts['robots'] = __( 'Not protected against Search engines DORK!', 'wprotect' );

            }

        }
        
        $extra_options['alerts'] = $alerts;

        ganjargal_updatePluginOption( 'extra_options', $extra_options );    

        wprotect_log( 'core', 'cron', __('Protection system is checked!', 'wprotect') );   

    }

    function getAlert( $return = 'onlyNum' )
    {

        $extra_options = ganjargal_getPluginOption('extra_options');

        if( is_array($extra_options['alerts']) && count($extra_options['alerts']) > 0 ){

            if( $return == 'onlyNum' ){

                return count($extra_options['alerts']);

            }elseif( $return == 'makeNotice'){

                if( $this->active_tab !== 'alerts'){

                    add_action( 'admin_notices', array( $this, 'makeAdminAlertNotices') );

                }

            }else{

                return $extra_options['alerts'];

            }

        }

        return;

    }

    private function removeAlert($alert)
    {

        $extra_options = ganjargal_getPluginOption('extra_options');

        if(is_array($extra_options['alerts']) && count($extra_options['alerts'])){

            if(isset($extra_options['alerts'][$alert])){

                unset($extra_options['alerts'][$alert]);

            }

        }

        ganjargal_updatePluginOption('extra_options', $extra_options);

    }

    public function sentAlerts()
    {

        $subject = __('Security alerts are detected!', 'wprotect');

        $message = '';

        $alerts = $this->getAlert('alerts');
        $fix_url = $this->alert_fix_urls;

        if(is_array($alerts) && count($alerts) > 0){

            $message .= '<h3>'.__('Security alerts are detected!', 'wprotect').'</h3>'."\n";

            $message .= '<ol>'."\n";

            foreach ($alerts as $key => $alert):

                $url = site_url().'/wp-admin/'.$fix_url[$key];

                $message .= '<li>'.$alert.' <a href="'.$url.'" target="_blank">'.__('Fix it', 'wprotect').'</a></li>'."\n";

            endforeach;

            $message .= '</ol>'."\n";

            ganjargal_sentMail($subject, $message);

        }

    }

    public function makeAdminAlertNotices()
    {

        $class = 'notice notice-error is-dismissible '._WPROTECT_TEXTDOMAIN;
        $message = strtoupper(_WPROTECT_TEXTDOMAIN).': '.sprintf( __( 'Security alerts are detected! <a href="%s">Click here</a> to fix it!', 'wprotect'), 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=alerts');

        printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message ); 

    }

    /********************** AJAX METHODS **********************/

    function ajax_protection_status()
    {

        check_ajax_referer(  $this->admin_nonces['protection_status'] ,'nonce' );

        if( current_user_can($this->capability) ){

            $response = array( 'result' => false );
            $statuses = array( 'on', 'off' );

            $protection = wp_unslash( $_POST['protection'] );
            $status = wp_unslash( $_POST['status'] );

            if( array_key_exists($protection, self::$options['protect_types']) &&
                in_array($status, $statuses) ){

                $saved_options = ganjargal_getPluginOption('protection_statuses');
                $saved_options[$protection] = $status;
                $response['result'] = ganjargal_updatePluginOption( 'protection_statuses', $saved_options );
                $title = self::$options['protect_types'][$protection]['title'];
                $response['status'] = $status;
                $response['text'] = strtoupper($title).__( ' protection is turned '.$status, 'wprotect' );

                $user_id = ganjargal_getLoggedUserID();

                wprotect_log( 'protection-status', $status, $response['text'], $user_id );

            }

            wp_send_json($response);
            die();

        }

    }

    function ajax_check_username()
    {
        global $wpdb;

        check_ajax_referer(  $this->admin_nonces['check_username'] ,'nonce' );

        if( current_user_can($this->capability) ){

            $response = array( 'valid' => false, 'message' => __('Sorry, Something went wrong! Please try again.', 'wprotect'));


            if(isset($_POST['username_admin']) && 
                $_POST['username_admin'] !== '' && 
                validate_username($_POST['username_admin'])){

                $username = esc_html($_POST['username_admin']);

                if( username_exists($username) ){

                    $response['message'] = __( 'Username already taken!', 'wprotect' );

                }else{

                    $response['valid'] = true;

                }

            }            

            wp_send_json($response);
            die();

        } 

    }

    function ajax_other_protections()
    {
        global $wpdb;

        check_ajax_referer(  $this->admin_nonces['other_protections'] ,'nonce' );
        
        if( current_user_can($this->capability) ){

            $response = array( 'result' => false, 'message' => __('Failed! Please check the file permission!', 'wprotect'));

            $protection = $_POST['protection'];

            $protection_directory = new WProtect_ProtectDirectories;

            switch($protection){

                case 'upload_phpkill':

                    $response = $protection_directory->uploadDir();

                    if($response['result']){

                        $this->removeAlert('upload_phpkill');

                    }

                break;

                case 'directory_listing':

                    $response = $protection_directory->directoryListing();

                    if($response['result']){

                        $this->removeAlert('directory_listing');

                    }

                break;
                    
                case 'robots':

                    $response = $protection_directory->robots();

                    if($response['result']){

                        $this->removeAlert('robots');

                    }

                break;

            }
            

            wp_send_json($response);
            die();

        } 

    }

    function ajax_remove_ban()
    {

        check_ajax_referer(  $this->admin_nonces['remove_ban'] ,'nonce' );

        if( current_user_can($this->capability) ){

            $response = array( 'result' => false, 'message' => __('Sorry, Something went wrong! Please try again.', 'wprotect') );
            $id = intval($_POST['id']);

            $ipban = new WProtect_BanIP;
            $response['result'] = $ipban->deleteBan($id);

            wp_send_json($response);
            die();

        }

    }

    function ajax_exdb_check()
    {

        check_ajax_referer(  $this->admin_nonces['exdb_check'] ,'nonce' );

        if( current_user_can($this->capability) ){

            $response = array( 'state' => false, 'message' => __('Sorry, Something went wrong! Please try again.', 'wprotect') );
            
            $exploitdb = new WProtect_ExploitDB;

            $user_id = ganjargal_getLoggedUserID();

            $response = $exploitdb->checkInfo($user_id);

            wp_send_json($response);
            die();

        }

    }

    function mainView()
    {
        
        $tab = $this->active_tab;

        if( isset($_POST[_WPROTECT_TEXTDOMAIN.'_save_option_nonce']) && 
            current_user_can($this->capability) && 
            $this->validNonce( _WPROTECT_TEXTDOMAIN.'_save_option_nonce', 'save_option' ) ){

            $this->saveOption( $tab );

        }

        $data = array();
        $data['tab'] = $this->blockgenerateTab();
        $data['saved_options'] = $this->statuses;
        $data['form_nonce'] = $this->admin_nonces['save_option'];
        $extra_options = ganjargal_getPluginOption('extra_options');
        $data['alerts'] = $this->getAlert('alerts');
        $data['logo'] = $this->blockLogo();

        switch($tab){

            case 'main':
            default:
                $data['alerts'] = $this->blockAlerts();
                $data['statuses'] = $this->blockStatuses(); 
                $data['logs'] = $this->blockLogs();               
                $view = 'main';
            break;

            case 'logs':
                $data['datas'] = $this->getLogs();
                $view = 'logs';
            break;

            case 'alerts':
                $data['fix_url'] = $this->alert_fix_urls;
                $data['alerts'] = $this->getAlert('alerts');
                $view = 'alerts';
            break;

            case 'hide':   
                $hideadmin = new WProtect_HideAdmin;
                $data['options_hideadmin'] = $hideadmin->getOptions();
                $data['hide_types'] = $hideadmin->getHideTypes();
                
                $hidelogin = new WProtect_HideLogin;
                $data['options_hidelogin'] = $hidelogin->getOptions();

                $data['ajax_nonce'] = $this->admin_nonces['protection_status'];
                $data['ajax_method'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[0];                
                $view = 'hide';
            break;

            case 'inputfilter':   
                $hideadmin = new WProtect_InputFilter;
                $data['options'] = $hideadmin->getOptions();             
                $data['ajax_nonce'] = $this->admin_nonces['protection_status'];
                $data['ajax_method'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[0];                
                $view = 'inputfilter';
            break;

            case 'bruteforce':   
                $bruteforce = new WProtect_Bruteforce;
                $data['options'] = $bruteforce->getOptions();             
                $data['ajax_nonce'] = $this->admin_nonces['protection_status'];
                $data['ajax_nonce_username'] = $this->admin_nonces['check_username'];
                $data['ajax_method'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[0];    
                $data['ajax_method_username'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[1];               
                $view = 'bruteforce';                
            break;

            case 'other':   
                $data['alerts'] = $this->getAlert('alerts');
                $data['ajax_nonce'] = $this->admin_nonces['other_protections'];
                $data['protection_directories'] = new WProtect_ProtectDirectories;
                $data['ajax_method'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[2];
                $data['ajax_nonce_setstatus'] = $this->admin_nonces['protection_status'];
                $data['ajax_method_setstatus'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[0];
                $view = 'other';
            break;

            case 'ban':
                $banip = new WProtect_BanIP;
                $data['options'] = $banip->getOptions();
                $data['ajax_nonce_status'] = $this->admin_nonces['protection_status'];
                $data['ajax_nonce'] = $this->admin_nonces['remove_ban'];
                $data['ajax_method'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[3];
                $data['ajax_method_status'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[0];
                $data['datas'] = $banip->getBans();
                $view = 'bans';
            break;

            case 'settings':
                $data['admin_themes'] = $this->admin_themes;
                $data['extra_options'] = $extra_options;
                $view = 'settings';
            break;

            case 'exploitdb':
                $exploitdb = new WProtect_ExploitDB;
                $data['options'] = $exploitdb->getOptions();      
                $data['ajax_nonce'] = $this->admin_nonces['protection_status'];
                $data['ajax_method'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[0];

                $data['ajax_nonce_check'] = $this->admin_nonces['exdb_check'];
                $data['ajax_method_check'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[4];

                $data['send_info'] = $exploitdb->getSendInfo();
                $view = 'exploitdb';
            break;

            case 'help':
                $view = 'help';
            break;

        }

        $this->loadView('admin', $view, $data);

    }

    /************************** BLOCKS *************************/

    protected function blockLogo()
    {

        $img = 'logo-normal.png';
        $tab = 'main';

        $alerts = intval($this->getAlert());

        if($alerts > 0){

            $img = 'logo-danger.png';
            $tab = 'alerts';

        }

        $path = _PLUGIN_URL_WPROTECT.'assets/img/'.$img;
        $link = 'admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab='.$tab;
        
        return array( 'img' => $path, 'link' => $link );

    }

    protected function blockGenerateTab()
    {

        $data = array();
        $data['active'] = $this->active_tab;
        $data['items'] = $this->admin_menus['sub'];

        $data['alerts'] = intval($this->getAlert('onlyNum'));

        return $this->loadView( 'admin'.DIRECTORY_SEPARATOR.'blocks', 'tab', $data, true );

    }

    protected function blockAlerts()
    {

        $data = array();
        
        $data['fix_url'] = $data['fix_url'] = $this->alert_fix_urls;
        $data['alerts'] = $this->getAlert('alerts');

        return $this->loadView( 'admin'.DIRECTORY_SEPARATOR.'blocks', 'alerts', $data, true );

    }

    protected function blockLogs()
    {

        $data = array();
        
        $data['logs'] = $this->getLogs(10);

        return $this->loadView( 'admin'.DIRECTORY_SEPARATOR.'blocks', 'logs', $data, true );

    }

    protected function blockStatuses()
    {

        $data = array();
        $data['protect_types'] = self::$options['protect_types'];
        $data['ajax_nonce'] = $this->admin_nonces['protection_status'];
        $data['ajax_method'] = _WPROTECT_TEXTDOMAIN.'_'.$this->ajax_methods[0];
        $data['saved_options'] = $this->statuses;

        return $this->loadView( 'admin'.DIRECTORY_SEPARATOR.'blocks', 'statuses', $data, true );

    }

    protected function loadView(  $folder = 'admin', $file=null, $data = array(), $buffer = false )
    {

        $data['textdomain'] = _WPROTECT_TEXTDOMAIN;

        $path = _PLUGIN_DIR_WPROTECT . DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$file.'.php';

        if(is_file( $path )){

            extract($data);

            $content = null;

            ob_start();

                require_once($path);
                $content = ob_get_contents();

            ob_end_clean();

            if($buffer){

                return $content;

            }else{

                echo $content;

            }

        }

    }

    public function loadJcss($hook=null)
    {

        wp_enqueue_style( _WPROTECT_TEXTDOMAIN.'-admin-css', _PLUGIN_URL_WPROTECT.'assets/css/admin/admin.css', array(), null );    

        wp_enqueue_script( _WPROTECT_TEXTDOMAIN.'-admin', _PLUGIN_URL_WPROTECT.'assets/js/admin/admin.js', array('jquery'), null, true ); 

        if($hook !== 'toplevel_page_'._WPROTECT_TEXTDOMAIN){
            return;
        }

        wp_enqueue_style( _WPROTECT_TEXTDOMAIN.'-admin-jtoggles', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/toggles/css/toggles.css', array(), null );
        wp_enqueue_style( _WPROTECT_TEXTDOMAIN.'-admin-jtoggles-theme', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/toggles/css/themes/toggles-light.css', array(), null );
        wp_enqueue_script( _WPROTECT_TEXTDOMAIN.'-admin-jtoggles-js', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/toggles/toggles.min.js', array('jquery'), null, true );

        wp_enqueue_style( _WPROTECT_TEXTDOMAIN.'-admin-toastr', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/toastr/toastr.min.css', array(), null );
        wp_enqueue_script( _WPROTECT_TEXTDOMAIN.'-admin-toastr-js', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/toastr/toastr.min.js', array('jquery'), null, true );

        wp_enqueue_style( _WPROTECT_TEXTDOMAIN.'-admin-validatr-theme', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/validate/validetta.css', array(), null );
        wp_enqueue_script( _WPROTECT_TEXTDOMAIN.'-admin-validatr-js', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/validate/validetta.min.js', array('jquery'), null, true );     

        wp_enqueue_style( _WPROTECT_TEXTDOMAIN.'-admin-tooltipster', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/tooltipster/css/tooltipster.bundle.min.css', array(), null );
        wp_enqueue_style( _WPROTECT_TEXTDOMAIN.'-admin-tooltipster-theme', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/tooltipster/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css', array(), null );
        wp_enqueue_script( _WPROTECT_TEXTDOMAIN.'-admin-tooltipster-js', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/tooltipster/js/tooltipster.bundle.min.js', array('jquery'), null, true ); 

        wp_enqueue_script( _WPROTECT_TEXTDOMAIN.'-admin-slimscroll-js', _PLUGIN_URL_WPROTECT.'assets/js/admin/plugins/jquery.slimscroll.min.js', array('jquery'), null, true );         

    }

    public function adminBodyClass( $classes )
    {

        $current_plugin = false;

        $current_tab = $this->active_tab;
        $current_page = @$_GET['page'];

        if(empty($current_tab)){

            $current_tab = $this->admin_menus['sub'][0]['tabid'];

        }

        foreach($this->admin_menus['sub'] as $item){

            if( $current_page == _WPROTECT_TEXTDOMAIN && $current_tab == $item['tabid']){

                $current_plugin = true;

            }

        }

        if($current_plugin){

            $extra_options = ganjargal_getPluginOption( 'extra_options' );
            $active_theme = $extra_options['theme_style'];

            $classes .= ' ' . _WPROTECT_TEXTDOMAIN .'-'. $active_theme .'-theme';

        }

        return $classes;

    }

    public function adminCreateMenu()
    {

        $parent = $this->admin_menus['parent'];

        add_menu_page(
            __( $parent['page_title'], _WPROTECT_TEXTDOMAIN ),
            __( $parent['menu_title'], _WPROTECT_TEXTDOMAIN ),
            $this->capability,
            $parent['menu_slug'],
            array($this, $parent['method']),
            $parent['icon'],
            $parent['position']
        );

        //$this->adminCreateSubMenu($parent);

    }

    public function adminCreateSubMenu($parent)
    {

        if(isset($this->admin_menus['sub'])){

            foreach($this->admin_menus['sub'] as $sub){

                $menu_slug = $sub['tabid'] == '' ? $parent['menu_slug'] : $sub['menu_slug'].'&tab='.$sub['tabid'];

                add_submenu_page(
                    $parent['menu_slug'],
                    __( $sub['page_title'], _WPROTECT_TEXTDOMAIN ),
                    __( $sub['menu_title'], _WPROTECT_TEXTDOMAIN ),
                    $this->capability,
                    $menu_slug,
                    array($this, $sub['method'])
                );

            }

        }

    }

    public function adminFooter()
    {
            $alerts = intval($this->getAlert());
            
        ?>
        <script type="text/javascript">
            var $wprotect_alerts = <?php echo $alerts; ?>;
        </script>                
        <div id="<?php echo _WPROTECT_TEXTDOMAIN; ?>-ajax-loader"></div>
        <?php

    }

    public function adminHead($hook)
    {

        $extra_options = ganjargal_getPluginOption('extra_options');

        if( $extra_options['onesignal_app_id'] !== '' && 
            $extra_options['onesignal_api_key'] !== '' &&
            $extra_options['onesignal_subdomain'] !== '' ){

            if( current_user_can($this->capability) ){

            ?>
            
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script>
<script type="text/javascript">

if(adminpage == 'toplevel_page_wprotect'){

    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "<?php echo esc_html($extra_options['onesignal_app_id']); ?>",
      autoRegister: false,
      subdomainName: '<?php echo esc_html($extra_options['onesignal_subdomain']); ?>',  
      <?php 
        if($extra_options['onesignal_safari_webid'] !== ''): 
        ?>
safari_web_id: '<?php echo esc_html($extra_options['onesignal_safari_webid']); ?>',
    <?php
        endif;
      ?>           
      httpPermissionRequest: {
        enable: true
      },
      notifyButton: {
          enable: true
      }
    }]);

}

</script>

            <?php

            }

        }

    }

}

}

if(!class_exists('WProtect')){

    class WProtect extends WProtectAdmin
    {
        
        function __construct()
        {

            parent::__construct();
            $this->liveProtecting();

        }

    }

}