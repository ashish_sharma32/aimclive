<?php

if(!defined('_CHKINC')){
    die;
}

class WProtect_HideLogin
{

	private $default_options = array(
										'key_name' 	=>	'wprotect',
										'key_value' => '12345'									
								);

	private $options = array();

	static private $option_field_name = 'hidelogin_options';

	function __construct()
	{

		$this->options = ganjargal_getPluginOption( self::$option_field_name );
        
        if( !is_array( $this->options ) || count($this->options) == 0){

            $this->options = $this->default_options;

        }

        if(!isset($this->options['key_name']) || $this->options['key_name'] == ''){

            $this->options['key_name'] = $this->default_options['key_name'];

        }

        if(!isset($this->options['key_value']) || $this->options['key_value'] == ''){

            $this->options['key_value'] = $this->default_options['key_value'];

        }

	}

	public function init()
	{

		add_option( _WPROTECT_TEXTDOMAIN.'_'.self::$option_field_name, $this->default_options, '', 'yes');

	}

	public function registerActions()
	{
		
		if ( ! function_exists( 'is_user_logged_in' ) ) {
			require_once ABSPATH . 'wp-includes/pluggable.php';
		}
					
		if( !is_user_logged_in() ){
            
			$filename = basename($_SERVER['PHP_SELF']);     	

			if($filename == 'wp-login.php'){

				if(isset($_GET['loggedout']) && $_GET['loggedout'] === 'true'){

					wp_redirect(home_url());
					exit;
					
				}

				$allow = false;

				if(isset($_SESSION['loginURLchecked'])){

					if( $_SERVER['REQUEST_METHOD'] === 'POST' ){

						$allow = true;

					}

				}

				if( isset($_GET[$this->options['key_name']]) && 
					$_GET[$this->options['key_name']] == $this->options['key_value']){

					$allow = true;
					$_SESSION['loginURLchecked'] = true;

				}

				if($allow == false){

					wprotect_log('hidelogin', 'off', sprintf( __('Direct access blocked to wp-login.php. IP address: %s', 'wprotect'), ganjargal_getClientIP()) );

					$this->show404();

				}

			}	       

		}

	}
    
    protected function show404()
    {
        global $wp_query;
        
        $wp_query->set_404();
        status_header( 404 );
        nocache_headers();
        
        if ( get_query_template('404') !== '' ){
            
            include( get_query_template( '404' ) );
            
        }else{
            
            include( _PLUGIN_DIR_WPROTECT .'views/404.php' );
            
        }

        exit;
                
    }

	static public function getOptionFieldName()
	{

		return self::$option_field_name;

	}
    
	public function getOptions()
	{

		return $this->options;

	}
    
    public function checkKey($key)
    {
        
        return array_key_exists($key, self::$hide_types);
        
    }

}