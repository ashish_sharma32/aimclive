<?php

if(!defined('_CHKINC')){
    die;
}

class WProtect_ProtectDirectories{

	private $open_line = '# WPROTECT BEGIN';
	private $close_line = '# END WPROTECT';
	private $rootDir = '';
	private $uploadDir = '';

	function __construct()
	{

		$this->rootDir = ABSPATH.DIRECTORY_SEPARATOR;

		$wp_upload_dir = wp_upload_dir();

        $this->uploadDir = $wp_upload_dir['basedir'].DIRECTORY_SEPARATOR;

	}

	public function checkUploadDirectory()
	{

		$path = $this->uploadDir.'.htaccess';

		if( is_file($path) ){

            if( strpos(@file_get_contents($path), '<Files *.php>') !== false ) {
                
                return true;

            }else{

            	return false;

            }

        }else{

        	return false;

        }

	}

	public function checkRootHtaccess()
	{

		$path = $this->rootDir.'.htaccess';

		if( is_file($path) ){

			if( strpos(@file_get_contents($path), '<Files *.sql>') !== false &&
				strpos(@file_get_contents($path), 'Options -Indexes') !== false) {
                
                return true;

            }else{

            	return false;

            }

		}else{

			return false;

		}

	}
    
    public function checkRobots()
    {
        
        $path = $this->rootDir.'robots.txt';

		if( is_file($path) ){

			if( strpos(@file_get_contents($path), 'Disallow: /wp-admin/') !== false &&
				strpos(@file_get_contents($path), 'Disallow: /wp-includes/') !== false) {
                
                return true;

            }else{

            	return false;

            }

		}else{

			return false;

		}

    }

	public function uploadDir()
	{	
		global $wp_filesystem;

		if (empty($wp_filesystem)) {
		    require_once (ABSPATH . '/wp-admin/includes/file.php');
		    WP_Filesystem();
		}

		$return = array( 'result' => false, 'message' => __('wp-content/uploads/.htaccess file is not writable!', 'wprotect') );

		$path = $this->uploadDir.'.htaccess';

		$commands  = "<Files *.php>\n";
		$commands .= "deny from all\n";
		$commands .= "</Files>\n";

		//$commands .= "RemoveHandler .php .phtml .php3\n";
		//$commands .= "RemoveType .php .phtml .php3\n";
		//$commands .= "php_flag engine off\n";

		$content = "\n".$this->open_line."\n";
		$content .= $commands;
		$content .= $this->close_line."\n";

		if($this->checkUploadDirectory()){

			$return['result'] = true;

			return $return;

		}else{

			if(!is_file($path)){

				if(fopen($path, "w")){

					$return['result'] = $wp_filesystem->put_contents($path, $content, 0644);

				}else{

					$return['message'] = __('wp-content/uploads/.htaccess file can not be created!', 'wprotect');

				}

			}else{

				if(is_writable($path)){

					$prev_content = @file_get_contents($path);

					if($prev_content){

						if (preg_match('/'.$this->open_line.'(.*?)'.$this->close_line.'/is', $prev_content, $matches)) {

							$new_content = preg_replace('/'.$this->open_line.'(.*?)'.$this->close_line.'/is', $content, $prev_content);

							$return['result'] =  $wp_filesystem->put_contents($path, $new_content, 0644);

						}else{

							$new_content = $content.$prev_content;
							$return['result'] = $wp_filesystem->put_contents($path, $new_content, 0644);

						}

					}else{

						$return['result'] = $wp_filesystem->put_contents($path, $content, 0644);

					}

				}

			}

		}

		if($return['result']){

			$user_id = ganjargal_getLoggedUserID();

			wprotect_log( 'protection-status', 'on', __('PHP execution in "wp-content/uploads" directory is prevented!', 'wprotect'), $user_id );

		}

		return $return;

	}

	public function directoryListing()
	{
		global $wp_filesystem;

		if (empty($wp_filesystem)) {
		    require_once (ABSPATH . '/wp-admin/includes/file.php');
		    WP_Filesystem();
		}

		$return = array( 'result' => false, 'message' => sprintf( __( '%s /.htaccess file is not writable!', 'wprotect' ), ABSPATH) );

		$path = $this->rootDir.'.htaccess';

		$commands  = "<Files *.sql>\n";
		$commands .= "deny from all\n";
		$commands .= "</Files>\n";
		$commands  .= "Options -Indexes\n";

		$content = "\n".$this->open_line."\n";
		$content .= $commands;
		$content .= $this->close_line."\n";

		if($this->checkRootHtaccess()){

			$return['result'] = true;

		}else{

			if(!is_file($path)){

				if(fopen($path, "w")){

					$return['result'] = $wp_filesystem->put_contents($path, $content, 0644);

				}else{

					$return['message'] = sprintf( __( '%s /.htaccess file can not be edited!', 'wprotect' ), ABSPATH);

				}

			}else{

				if(is_writable($path)){

					$prev_content = @file_get_contents($path);

					if($prev_content){

						if (preg_match('/'.$this->open_line.'(.*?)'.$this->close_line.'/is', $prev_content, $matches)) {

							$new_content = preg_replace('/'.$this->open_line.'(.*?)'.$this->close_line.'/is', $content, $prev_content);

							$return['result'] =  $wp_filesystem->put_contents($path, $new_content, 0644);

						}else{

							$new_content = $content.$prev_content;
							$return['result'] = $wp_filesystem->put_contents($path, $new_content, 0644);

						}

					}else{

						$return['result'] = $wp_filesystem->put_contents($path, $content, 0644);

					}

				}

			}

		}

		if($return['result']){

			$user_id = ganjargal_getLoggedUserID();

			wprotect_log( 'protection-status', 'on', __('Directory listing is disabled!', 'wprotect'), $user_id );

		}

		return $return;

	}

    public function robots()
	{
		global $wp_filesystem;

		if (empty($wp_filesystem)) {
		    require_once (ABSPATH . '/wp-admin/includes/file.php');
		    WP_Filesystem();
		}

		$return = array( 'result' => false, 'message' => sprintf( __( '%s /robots.txt file is not writable!', 'wprotect' ), ABSPATH) );

		$path = $this->rootDir.'robots.txt';

		$commands  = "User-agent: *\n";
        $commands  .= "Disallow: /wp-admin/\n";
        $commands  .= "Disallow: /wp-includes/\n";
        $commands  .= "Disallow: /trackback/\n";
        $commands  .= "Disallow: /xmlrpc.php\n";
        $commands  .= "Disallow: /wp-content/plugins/\n";
        
		$content = "\n".$this->open_line."\n";
		$content .= $commands;
		$content .= $this->close_line."\n";

		if($this->checkRobots()){

			$return['result'] = true;

		}else{

			if(!is_file($path)){

				if(fopen($path, "w")){

					$return['result'] = $wp_filesystem->put_contents($path, $content, 0644);

				}else{

					$return['message'] = sprintf( __( '%s /robots.txt file can not be edited!', 'wprotect' ), ABSPATH);

				}

			}else{

				if(is_writable($path)){

					$prev_content = @file_get_contents($path);

					$prev_content = @file_get_contents($path);

					if($prev_content){

						if (preg_match('/'.$this->open_line.'(.*?)'.$this->close_line.'/is', $prev_content, $matches)) {

							$new_content = preg_replace('/'.$this->open_line.'(.*?)'.$this->close_line.'/is', $content, $prev_content);

							$return['result'] =  $wp_filesystem->put_contents($path, $new_content, 0644);

						}else{

							$new_content = $content.$prev_content;
							$return['result'] = $wp_filesystem->put_contents($path, $new_content, 0644);

						}

					}else{

						$return['result'] = $wp_filesystem->put_contents($path, $content, 0644);

					}

				}

			}

		}

		if($return['result']){

			$user_id = ganjargal_getLoggedUserID();

			wprotect_log( 'protection-status', 'on', __('Protected against Search engines dork!', 'wprotect'), $user_id );

		}

		return $return;

	}

}