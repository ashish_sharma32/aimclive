<?php

if(!defined('_CHKINC')){
    die;
}

class WProtect_HideAdmin
{

	private $default_options = array(
										'hide_type' 	=>	'go2home'									
								);

	private $options = array();

	static private $option_field_name = 'hideadmin_options';

	private $allowed_requests = array (
										'admin-ajax.php', 
                                        'async-upload.php', 
                                        'wp-app.php'
									);
    static private $hide_types = array(
                                        'go2home' => 'Redirect to home page', 
                                        'show404' => 'Show 404 page'
                                    );

	function __construct()
	{

		$this->options = ganjargal_getPluginOption( self::$option_field_name );
        
        if( !is_array( $this->options ) || count($this->options) == 0){

            $this->options = $this->default_options;

        }

        if(!isset($this->options['hide_type'])){

            $this->options['hide_type'] = $this->default_options['hide_type'];

        }

	}

	public function init()
	{

		add_option( _WPROTECT_TEXTDOMAIN.'_'.self::$option_field_name, $this->default_options, '', 'yes');

	}

	public function registerActions()
	{

		if( $this->isRedirect() ){

			if ( ! function_exists( 'is_user_logged_in' ) ) {
				require_once ABSPATH . 'wp-includes/pluggable.php';
			}
						
			if( is_admin() && !is_user_logged_in() ){
                
                if($this->options['hide_type'] == 'go2home'){
                    
                    wp_redirect(home_url('/'));
				    exit;
                    
                }else{
                    
                    $this->show404();                    
                    
                }

			}

		}

	}
    
    protected function show404()
    {
        global $wp_query;
        
        $wp_query->set_404();
        status_header( 404 );
        nocache_headers();
        
        if ( get_query_template('404') !== '' ){
            
            include( get_query_template( '404' ) );
            
        }else{
            
            include( _PLUGIN_DIR_WPROTECT .'views/404.php' );
            
        }

        exit;
                
    }

	protected function isRedirect()
	{

		$filename = basename($_SERVER['PHP_SELF']);

		if ( defined( 'WP_CLI' ) and WP_CLI ) {

			return false;

		}

		if( in_array($filename, $this->allowed_requests) ){

			return false;

		}

		return true;

	}

	static public function getOptionFieldName()
	{

		return self::$option_field_name;

	}
    
    public function getHideTypes()
    {
        
        return self::$hide_types;
        
    }

	public function getOptions()
	{

		return $this->options;

	}
    
    public function checkType($type)
    {
        
        return array_key_exists($type, self::$hide_types);
        
    }

}