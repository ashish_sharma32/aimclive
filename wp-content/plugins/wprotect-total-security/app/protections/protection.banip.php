<?php

if(!defined('_CHKINC')){
    die;
}

class WProtect_BanIP{

	private $default_options = array(

                                'ban_time' => 24 //hours

                            );

	private $options = array();

	private $table_name = 'bans';

	static private $option_field_name = 'banip_options';

	function __construct()
	{
		global $wpdb;

		$this->table_name = $wpdb->prefix._WPROTECT_TEXTDOMAIN.'_'.$this->table_name;

		$this->options = ganjargal_getPluginOption( self::$option_field_name );
        
        if( !is_array( $this->options ) || count( $this->options) == 0){

            $this->options = $this->default_options;

        }

	}

	function init()
	{
		global $wpdb;

        $sql = "CREATE TABLE IF NOT EXISTS ".$this->table_name." (
                      `ID` bigint(20) NOT NULL AUTO_INCREMENT,
                      `ip_address` varchar(39) DEFAULT NULL,
                      `created_at` int(11) DEFAULT NULL,
                      `end_time` int(11) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                );";

        if( !function_exists('dbDelta')){

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        }

        dbDelta( $sql );

        add_option( _WPROTECT_TEXTDOMAIN.'_'.self::$option_field_name, $this->default_options, '', 'yes');

	}

	public function registerActions()
	{	
		
		$this->clearOldBans();

		if($this->isBanned()){

			status_header( 403 );
        	nocache_headers();
			exit( __('Your IP address is blocked!', 'wprotect') );

		}

	}

	public function getOptions()
    {

        return $this->options;

    }

    static public function getOptionFieldName()
    {

        return self::$option_field_name;

    }

    public function isBanned($ip=null)
    {
    	global $wpdb;

    	$ip = is_null($ip) ? ganjargal_getClientIP() : $ip;

    	$count_ip = $wpdb->get_var( $wpdb->prepare( 
			"SELECT COUNT(*) 
			 FROM ".$this->table_name." 
			 WHERE ip_address = %s 
			 LIMIT 1
			", 
			$ip
		) );

		if(intval($count_ip) == 0){

			return false;

		}else{

			return true;

		}

    }

    public function addBanIp($ip=null, $time=null)
    {
    	global $wpdb;

    	$ip = is_null($ip) ? ganjargal_getClientIP() : $ip;

        if(!@inet_pton($ip)){
            return;
        }

		if(!$this->isBanned($ip)){	

			$created_at = time();
			$end_time = is_null($time) ? $created_at + $this->options['ban_time'] * 3600 : $created_at + $time * 60;

			$result = $wpdb->insert(
                $this->table_name,
                array(
                        'ip_address' => $ip,
                        'created_at' => $created_at,
                        'end_time' => $end_time
                ),
                array(
                        '%s',
                        '%d',
                        '%d'
                )
            );

            if($result !== false){

                return $ip;

            }

		}

    }

    public function deleteBan($value)
    {
    	global $wpdb;

        $column = 'ip_address';

        if(is_int($value) && $value > 0){

            $column = 'ID';

        }

    	return $wpdb->query( 
            $wpdb->prepare( 
                "
                DELETE FROM ".$this->table_name."
                WHERE ".$column." = %s 
                LIMIT 1
                ",
                    $value
                )
        );

    }

    private function clearOldBans()
    {
    	global $wpdb;

    	$wpdb->query( 
            $wpdb->prepare( 
                "
                DELETE FROM ".$this->table_name."
                WHERE end_time < %d",
                    time()
                )
        );    	

    }

    public function getBans()
    {
    	global $wpdb;

		$query = "SELECT COUNT(1) FROM ".$this->table_name." AS combined_table";
     	$total = $wpdb->get_var( $query );

     	$items_per_page = 20;

     	$page = isset( $_GET['paged'] ) ? abs( (int) $_GET['paged'] ) : 1;

     	$offset = ( $page * $items_per_page ) - $items_per_page;

     	$rows =  $wpdb->get_results("
     			SELECT * FROM ".$this->table_name." 
     			ORDER BY created_at DESC LIMIT ".$offset.", ".$items_per_page);

     	$pagination = paginate_links( array(
					        'base' => add_query_arg( 'paged', '%#%' ),
					        'format' => '',
					        'prev_text' => __('&laquo;'),
					        'next_text' => __('&raquo;'),
					        'total' => ceil($total / $items_per_page),
					        'current' => $page
					    ));

     	return array( 'rows' => $rows, 'pagination' => $pagination, 'offset' => $offset );
    }

}