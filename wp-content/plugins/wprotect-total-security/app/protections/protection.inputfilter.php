<?php

if(!defined('_CHKINC')){
    die;
}

/**
 * Inspired from Codeigniter Input & Security classes.
 */

include_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.inputfilter.php' );

class WProtect_InputFilter{

	private $default_options = array(
										'enable_xss' 	=>	'on',
										'filter_get' 	=>	'on',
										'filter_post'	=>	'on',
										'filter_cookies'	=>	'on',
										'filter_file_inclusion'	=>	'on'
									);

    private $options = array();

	static private $option_field_name = 'inputfilter_options';

	function __construct()
	{

		$this->options = ganjargal_getPluginOption( self::$option_field_name );
        
        if( !is_array( $this->options ) || count($this->options) == 0){

            $this->options = $this->default_options;

        }

        if(!isset($this->options['enable_xss'])){

            $this->options['enable_xss'] = $this->default_options['enable_xss'];

        }

        if(!isset($this->options['filter_get'])){

            $this->options['filter_get'] = $this->default_options['filter_get'];

        }

        if(!isset($this->options['filter_post'])){

            $this->options['filter_post'] = $this->default_options['filter_post'];

        }

        if(!isset($this->options['filter_cookies'])){

            $this->options['filter_cookies'] = $this->default_options['filter_cookies'];

        }

        if(!isset($this->options['filter_file_inclusion'])){

            $this->options['filter_file_inclusion'] = $this->default_options['filter_file_inclusion'];

        }

	}


	public function init()
	{

		add_option( _WPROTECT_TEXTDOMAIN.'_'.self::$option_field_name, $this->default_options, '', 'yes');

	}

	public function registerActions()
	{	

		if( !is_admin() ){
			
			add_action( 'init', array( $this, 'requestFilter' ), 1 );
		
		}

	}

	public function requestFilter()
	{
		
		$filter = new WProtectInputFilter( $this->options['enable_xss'] );

		if ($this->options['filter_get'] == 'on' && is_array($_GET) && count($_GET) > 0)
		{
			foreach ($_GET as $key => $val)
			{
				$_GET[$filter->_clean_input_keys($key)] = $filter->_clean_input_data($val);
			}
		}

		if( $this->options['filter_post'] == 'on' && is_array($_POST) && count($_POST) > 0)
		{

			foreach ($_POST as $key => $value) {
				
				$_POST[$filter->_clean_input_keys($key)] = $filter->_clean_input_data($value);

			}

		}

		if ( $this->options['filter_cookies'] == 'on' && is_array($_COOKIE) && count($_COOKIE) > 0)
		{

			foreach ($_COOKIE as $key => $val)
			{
				$_COOKIE[$filter->_clean_input_keys($key)] = $filter->_clean_input_data($val);
			}

		}

		$_SERVER['PHP_SELF'] = strip_tags($_SERVER['PHP_SELF']);
		
		return;

	}

	public function getOptions()
	{

		return $this->options;

	}

	public static function getOptionFieldName()
	{

		return self::$option_field_name;

	}

}