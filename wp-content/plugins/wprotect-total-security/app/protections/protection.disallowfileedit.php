<?php

if(!defined('_CHKINC')){
    die;
}

class WProtect_DisallowFileEdit
{


	function __construct()
	{
		return;
	}

	public function init()
	{
		return true;
	}

	public function registerActions()
	{

		$action = true;

		if( defined('DISALLOW_FILE_EDIT') ){

			if(DISALLOW_FILE_EDIT == true){

				$action = false;

			}

		}

		if($action){

			define('DISALLOW_FILE_EDIT', true);

		}

	}
	
}