<?php

if(!defined('_CHKINC')){
    die;
}

include_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.recaptcha.php' );

class WProtect_Bruteforce{

    private $default_options = array(

                                'attempts_limit' => 5 ,
                                'attempt_minutes' => 30

                            );

    private $options = array();

    private $table_name = 'login_attempts';

    static private $option_field_name = 'bruteforce_options';

    function __construct()
    {
        global $wpdb;

        $this->table_name = $wpdb->prefix._WPROTECT_TEXTDOMAIN.'_'.$this->table_name;

        $this->options = ganjargal_getPluginOption( self::$option_field_name );
        
        if( !is_array( $this->options ) || count( $this->options) == 0){

            $this->options = $this->default_options;

        }

        if(!isset($this->options['attempt_minutes'])){

            $this->options['attempt_minutes'] = $this->default_options['attempt_minutes'];

        }

        if(!isset($this->options['attempts_limit'])){

            $this->options['attempts_limit'] = $this->default_options['attempts_limit'];

        }

        if(intval($this->options['attempts_limit']) < 1){

            $this->options['attempts_limit'] = $this->default_options['attempts_limit'];

        }

        $extra_options = ganjargal_getPluginOption( 'extra_options' );

        $this->options['recaptcha_site_key'] = @$extra_options['recaptcha_site_key'];
        $this->options['recaptcha_secret_key'] = @$extra_options['recaptcha_secret_key'];

    }

    public function init()
    {
        global $wpdb;

        $sql = "CREATE TABLE IF NOT EXISTS ".$this->table_name." (
              `ID` bigint(20) NOT NULL AUTO_INCREMENT,
              `ip_address` varchar(100) DEFAULT NULL,
              `attemp_count` int(2) DEFAULT NULL,
              `last_updated` int(11) DEFAULT NULL,
              PRIMARY KEY (`ID`)
        );";

        if( !function_exists('dbDelta')){

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        }
        dbDelta( $sql );

        add_option( _WPROTECT_TEXTDOMAIN.'_'.self::$option_field_name, $this->default_options, '', 'yes');

    }

    public function registerActions()
    {
        
        add_action( 'wp_login_failed', array( $this, 'loginFailed' ), 10, 1 );
        add_action( 'wp_login', array( $this, 'loginSuccess' ), 10, 1 );
        add_action( 'wp_authenticate_user', array( $this, 'verifyLogin' ), 10, 2 );

        $this->checkAttempt();

    }

    protected function clearOldAttempts()
    {
        global $wpdb;

        $now = time();

        $convert = intval($this->options['attempt_minutes']) * 60;
        $calculate = $now - $convert;

        $wpdb->query( 
            $wpdb->prepare( 
                "
                DELETE FROM ".$this->table_name."
                WHERE last_updated < %d
                ",
                    $calculate
                )
        );

    }

    public function verifyLogin($user, $password)
    {

        $row = $this->attemptRow();

        if($row && $this->isFailed( intval($row->attemp_count) )){

            return WprotectRecaptcha::verifyCaptcha( $this->options['recaptcha_secret_key'], array( $user, $password ) );

        }

        return $user;

    }

    protected function checkAttempt( $return = true )
    {

        $row = $this->attemptRow();

        if($row && $this->isFailed( intval($row->attemp_count) )){

            $this->showCaptcha();

        }

    }

    protected function attemptRow()
    {
        global $wpdb;

        $this->clearOldAttempts();

        $return = false;

        $row = $wpdb->get_row( "SELECT * FROM ".$this->table_name."  
                                WHERE ip_address = '".ganjargal_getClientIP()."' LIMIT 1" );

        if($row){

            $return = $row;

        }

        return $return;

    }

    protected function isFailed($numAttept=1)
    {

        if( intval($numAttept) < intval($this->options['attempts_limit'])  ){

            return false;

        }else{

            return true;

        }

    }

    protected function increaseAttempt($numAttept=1)
    {
        global $wpdb;

        $increase_attempt = $numAttept + 1;

        $wpdb->update(
                $this->table_name,
                array(
                        'ip_address' => ganjargal_getClientIP(),
                        'attemp_count' => $increase_attempt,
                        'last_updated' => time()
                ),
                array( 'ip_address' => ganjargal_getClientIP() ),
                array(
                        '%s',
                        '%d',
                        '%d'
                )
            );

        return $increase_attempt;

    }

    protected function addAttempt()
    {   
        global $wpdb;

        $wpdb->insert(
                $this->table_name,
                array(
                        'ip_address' => ganjargal_getClientIP(),
                        'attemp_count' => 1,
                        'last_updated' => time()
                ),
                array(
                        '%s',
                        '%d',
                        '%d'
                )
            );
    }

    public function loginFailed($username)
    {
        global $wpdb;

        $row = $this->attemptRow();

        if($row){

            if( !$this->isFailed( intval($row->attemp_count)) ){

                $increase = $this->increaseAttempt( intval($row->attemp_count) );

                if( $this->isFailed( $increase ) ){

                    $this->showCaptcha();

                }

            }else{

                $this->showCaptcha();

            }

        }else{

            $this->addAttempt();

        }

    }    

    public function getOptions()
    {

        return $this->options;

    }

    static public function getOptionFieldName()
    {

        return self::$option_field_name;

    }

    public function loginSuccess($user_login=null, $user=null)
    {
        global $wpdb;

        $wpdb->delete(
                $this->table_name,
                array( 'ip_address' => ganjargal_getClientIP() ),
                array( '%s' )
        );
    }

    protected function showCaptcha()
    {

        $ip = ganjargal_getClientIP();
        
        $title = __('Brute-force attack detected!');
        $url = home_url().'/wp-admin/admin.php?page='._WPROTECT_TEXTDOMAIN.'&tab=logs';

        if($this->options['recaptcha_secret_key'] == '' || $this->options['recaptcha_site_key'] == ''){

            $banip = new WProtect_BanIP;

            if( $_SERVER['REQUEST_METHOD'] === 'POST' && $banip->isBanned($ip) == false){

                $banip->addBanIp($ip, $this->options['attempt_minutes']);
                
                $message = sprintf( __('Brute force attack is detected & banned the IP address: %s', 'wprotect'), $ip);

                wprotect_log( 'core', 'bannedip', $message );

                ganjargal_pushNotification($title, $message, $url);

                return true;

            }

        }else{

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $message = sprintf( __( 'Brute force attack is detected from IP address: %s', 'wprotect'), $ip );

                wprotect_log( 'bruteforce', 'off', $message );

                ganjargal_pushNotification($title, $message, $url);

            }
            
        }

        

        add_filter( 'login_head', array( $this, 'attachLoginHead' ) );
        add_action( 'login_form', array( $this, 'addCaptcha' ) );
        add_filter( 'login_head', array( 'WprotectRecaptcha', 'attachHead' ) );
    }
    
    public function attachLoginHead()
    {

?>    
    <style type="text/css">
        #login{
            width: 354px;
        }
        #login .g-recaptcha{
            margin-bottom: 20px;
        }
    </style>
<?php
    }

    public function addCaptcha()
    {
    ?>
    <p>
        <?php WprotectRecaptcha::drawCaptcha( $this->options['recaptcha_site_key'] ); ?>
    </p>
    <?php
    }

}
