<?php

if(!defined('_CHKINC')){
    die;
}

class WProtect_HideVersion
{

	function __construct()
	{
		return;
	}

	public function init()
	{
		return true;
	}

	public function registerActions()
	{

		global $wp_styles;

		remove_action('wp_head', 'wp_generator');		
		add_filter('the_generator', array($this, 'removeVersion'));

		add_filter( 'style_loader_src', array($this, 'removeVersionVar'), 9999 );
		add_filter( 'script_loader_src', array($this, 'removeVersionVar'), 9999 );

	}

	public function replaceBlogInfo( $text, $show )
	{

		if($show == 'version'){

			$text = date('Ymd');

		}

		return $text;

	}

	public function removeVersion()
	{

		return '';

	}

	public function removeVersionVar( $src )
	{

		if ( strpos( $src, 'ver=' ) ){

			$src = remove_query_arg( 'ver', $src );
			$src = add_query_arg('ver', date('Ymd'), $src);
		}

		return $src;

	}
	
}