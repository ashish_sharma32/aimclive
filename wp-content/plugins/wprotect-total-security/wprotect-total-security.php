<?php

/*
Plugin Name: WProtect - Total Security Plugin
Plugin URI: http://www.ganjargal.pro/
Description: WProtect is a global protection suite designed to protect your website without affecting your server's speed.
Author: Ganjargal Bolor
Text Domain: wprotect
Version: 1.0.1
Author URI: http://www.ganjargal.pro/
*/

if ( !defined( 'WPINC' ) ) {
     die;
}

if( !defined('_CHKINC') ){
    define('_CHKINC', true);
}

if ( !defined('_PLUGIN_FILE_WPROTECT') ){
    define( '_PLUGIN_FILE_WPROTECT', __FILE__ );
}

if ( !defined('_PLUGIN_DIR_WPROTECT') ){
    define( '_PLUGIN_DIR_WPROTECT', plugin_dir_path( __FILE__ ) );
}

if ( !defined('_PLUGIN_URL_WPROTECT') ){
    define( '_PLUGIN_URL_WPROTECT', plugin_dir_url( __FILE__ ) );
}

if( !defined('_WPROTECT_TEXTDOMAIN') ){
    define('_WPROTECT_TEXTDOMAIN', 'wprotect');
}

require_once( _PLUGIN_DIR_WPROTECT.'app'.DIRECTORY_SEPARATOR.'helpers.php' );
  
WProtectLoadProtections(); 

WProtect::init();



