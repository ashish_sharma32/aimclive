jQuery(document).ready(function(){

	var $class = 'wprotect-success';

	if(jQuery('body').hasClass('toplevel_page_wprotect')){

		var $loader = jQuery('#wprotect-ajax-loader');

		jQuery( document ).ajaxStart(function() {
		  $loader.show();
		});

		jQuery( document ).ajaxStop(function() {
		  $loader.hide();
		});

	}

	if(	typeof $wprotect_alerts !== 'undefined' && 
		parseInt($wprotect_alerts) > 0	){

		var $object = jQuery('#toplevel_page_wprotect').children('a').find('.wp-menu-name');
		var $name = $object.text();
		$object.append( jQuery('<span class="update-plugins"><span class="plugin-count">'+parseInt($wprotect_alerts)+'</span></span>') );
		
		$class = 'wprotect-warning';

	}

	jQuery('#toplevel_page_wprotect').addClass($class);

	jQuery('#nav-selector').change(function(e){

		var $url = jQuery(this).val();
		document.location.href = $url;

	});

});