<?php
/**
 * Plugin Name: Social Rail
 * Plugin URI: http://www.socialrail.info/wp/
 * Description: WordPress Facebook and Twitter social stream plugin.
 * Version: 1.0
 * Author: Federico Schiocchet - Pixor
 * Author URI: http://www.pixor.it/
 */

define("SR_PLUGIN_URL", plugins_url() . "/socialrail");
function sr_enqueue() {
    wp_register_style("sr-main-css", SR_PLUGIN_URL . "/include/main.css", array(), "1.0", "all");
    wp_register_style("sr-slider-css", SR_PLUGIN_URL . "/include/flexslider/flexslider.css", array(), "1.0", "all");
    wp_register_script("sr-main-js", SR_PLUGIN_URL . "/include/main.js", array("jquery"), "1.0");
    wp_register_script("sr-slider-js", SR_PLUGIN_URL . "/include/flexslider/flexslider.js", array(), "1.0");
    wp_register_script("sr-scrollbox-js", SR_PLUGIN_URL . "/include/slimscroll.js", array(), "1.0");
}
add_action('wp_enqueue_scripts', 'sr_enqueue');

//MENU
function sr_set_admin_menu() {
    add_submenu_page("options-general.php", "Social Rail", "Social Rail", "administrator", "social-rail","sr_set_page_menu_link");
}
function sr_set_page_menu_link() {
    $arr = array("","","","","");
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['submit'])) {
            $fb_access_token= "null";
            $tw_consumer_key = "null";
            $tw_consumer_secret = "null";
            $tw_access_token = "null";
            $tw_access_secret = "null";

            if ($_POST['fb-access-token'] != "") $fb_access_token = $_POST['fb-access-token'];
            if ($_POST['tw-consumer-key'] != "") $tw_consumer_key = $_POST['tw-consumer-key'];
            if ($_POST['tw-consumer-secret'] != "") $tw_consumer_secret = $_POST['tw-consumer-secret'];
            if ($_POST['tw-access-token'] != "") $tw_access_token = $_POST['tw-access-token'];
            if ($_POST['tw-access-secret'] != "") $tw_access_secret = $_POST['tw-access-secret'];

            update_option("sr_settings", $fb_access_token . "/" . $tw_consumer_key . "/" . $tw_consumer_secret . "/" . $tw_access_token . "/" . $tw_access_secret);
        }


    }
    $arr = get_option("sr_settings");
    if ($arr != "" && $arr != false) $arr = explode("/",$arr);
    else $arr =array("","","","","");
    for ($i = 0; $i < count($arr); $i++){
        if ($arr[$i] == "null") $arr[$i] = "";
    }
?>
<div class="wrap">
    <h1>Social Rail</h1>
    <p>
        <?php _e("Need help? Open the onlinde documentation at <a target='_blank' href='http://www.socialrail.info/wp/doc/'>www.socialrail.info/wp/doc</a>.","sr") ?>
    </p>
    <form method="post" action="">
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">Facebook Access Token</th>
                    <td>
                        <input name="fb-access-token" type="text" id="fb-access-token" value="<?php echo $arr[0] ?>" class="regular-text ltr" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Twitter Consumer Key</th>
                    <td>
                        <input name="tw-consumer-key" type="text" id="tw-consumer-key" value="<?php echo $arr[1] ?>" class="regular-text ltr" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Twitter Consumer Secret</th>
                    <td>
                        <input name="tw-consumer-secret" type="text" id="tw-consumer-secret" value="<?php echo $arr[2] ?>" class="regular-text ltr" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Twitter Access Token</th>
                    <td>
                        <input name="tw-access-token" type="text" id="tw-access-token" value="<?php echo $arr[3] ?>" class="regular-text ltr" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Twitter Access Secret</th>
                    <td>
                        <input name="tw-access-secret" type="text" id="tw-access-secret" value="<?php echo $arr[4] ?>" class="regular-text ltr" />
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <br />
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes" />
    </form>
</div>

<?php }
add_action("admin_menu", "sr_set_admin_menu");

//SHORTCODE
function social_rail($atts)  {
    $html = "";
    $css = "";
    $arr = get_option("sr_settings");
    if ($arr != "" && $arr != false) $arr = explode("/",$arr);
    else $arr = "";
    extract(shortcode_atts(array(
            'site' => 'fb',
            'type' => 'base',
            'style' => '',
            'options' => '',
            'trigger' => '',
            'height' => '',
            'offset' => ''
        ), $atts));

    ob_start();
    if (!isset($atts["site"])) $atts["site"] = "";
    if (!isset($atts["type"])) $atts["type"] = "";
    if (!isset($atts["style"])) $atts["style"] = "";
    if (!isset($atts["options"])) $atts["options"] = "";
    if (!isset($atts["trigger"])) $atts["trigger"] = "";
    if (!isset($atts["height"])) $atts["height"] = "";
    if (!isset($atts["offset"])) $atts["offset"] = "";

    if ($atts["options"] != "") {
        $html = 'data-options="' . $atts["options"] . '"';
    }
    if ($atts["trigger"] != "") {
        $html .= ' data-trigger="' . $atts["trigger"] . '"';
    }
    if ($atts["height"] != "") {
        $html = ' data-height="' . $atts["height"] . '"';
    }
    if ($atts["offset"] != "") {
        $html = ' data-height-remove="' . $atts["offset"] . '"';
    }
    if ($atts["type"] == "slider") $css = "flexslider";
    if ($atts["type"] == "carousel") $css = "flexslider carousel";
    if ($atts["type"] == "scroll") {
        $css = "scroll-content";
        wp_enqueue_script('sr-scrollbox-js');
        $html .= ' data-height="' . (($atts["height"] == "") ? "300" : $atts["height"]) . '"';
        $html .= ' data-remove-height="' . (($atts["offset"] == "") ? "0" : $atts["offset"]) . '"';
    }
    if ($atts["site"] == "fb") {
        if ($arr != "" && $arr[0] != "null") $html .= ' data-token="' . $arr[0] . '"';
    }
    if ($atts["style"] == "boxed") $css .= " feed-boxed";
    if ($atts["style"] == "dark") $css .= " feed-dark";
    if ($atts["style"] == "dark boxed") $css .= " feed-dark feed-boxed";
    echo '<div class="social-feed-' . $atts["site"] . ' ' . $css . '" data-social-id="' . $atts["id"] . '" ' . $html . '></div>';
    wp_enqueue_style('sr-main-css');
    wp_enqueue_script('sr-main-js');
    if ($atts["type"] == "slider" || $atts["type"] == "carousel") {
        wp_enqueue_style('sr-slider-css');
        wp_enqueue_script('sr-slider-js');
    }
    $output = ob_get_clean();
    return $output;
}
add_shortcode('sr', 'social_rail');
?>
