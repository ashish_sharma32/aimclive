<?php 
    $output = ''; 
    $sub_img = array();
    global $post;
    if(!function_exists('politist_call_sub_header')){
        function politist_call_sub_header(){
            if( politist_options_url('blog-banner','url')){
                $output = 'style="background-image:url('.esc_url( politist_options_url('blog-banner','url')).');background-size: cover;background-position: 50% 50%;"';
                return $output;
            }else{
                if( politist_options('blog-subtitle-bg-color') ){
                    $output = 'style="background-color:'.esc_attr(politist_options('blog-subtitle-bg-color')).';"';
                    return $output;
                }else{
                    $output = 'style="background-color:#151515;"';
                    return $output;
                }
            }
        }
    }
    
    if( isset($post->post_name) ){
        if(!empty($post->ID)){
            if(function_exists('rwmb_meta')){
                $image_attached = esc_attr(get_post_meta( $post->ID , 'themeum_subtitle_images', true ));
                if(!empty($image_attached)){
                    $sub_img = wp_get_attachment_image_src( $image_attached , 'blog-full'); 
                    $output = 'style="background-image:url('.esc_url($sub_img[0]).');background-size: cover;background-position: 50% 50%;"';
                    if(empty($sub_img[0])){
                        $output = 'style="background-color:'.esc_attr(rwmb_meta("themeum_subtitle_color")).';"';
                        if(rwmb_meta("themeum_subtitle_color") == ''){
                            $output = politist_call_sub_header();
                        }
                    }
                }else{
                    if(rwmb_meta("themeum_subtitle_color") != "" ){
                        $output = 'style="background-color:'.esc_attr(rwmb_meta("themeum_subtitle_color")).';"';
                    }else{
                        $output = politist_call_sub_header();
                    }
                }
            }else{
                $output = politist_call_sub_header();
            } 
        }else{
            $output = politist_call_sub_header();
        }
    }else{
        $output = politist_call_sub_header();
    }

?>
<?php if (!is_front_page()) { ?>

<div class="breadcrumbs-area" <?php echo $output;?>>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="bread-content text-left">
                    <?php 
                    if (!is_single() ) {
                        global $wp_query; 
                        if(isset($wp_query->queried_object->name)){
                            if($wp_query->queried_object->name != ''){
                                if($wp_query->queried_object->name == 'product' ){
                                    echo '<h2>'.esc_html__('Shop','politist').'</h2>';
                                } elseif($wp_query->queried_object->name == 'tribe_events' )  {
                                    echo '<h2>'.esc_html__('Events','politist').'</h2>';
                                }
                                else{
                                    echo '<h2 class="test">'.$wp_query->queried_object->name.'</h2>';    
                                }
                            }else{
                                echo '<h2>'.get_the_title().'</h2>';
                            }
                        }else{
                            if( is_search() ){
                                echo '<h2>'.esc_html__('Shop','politist').'</h2>';
                            }else{
                                echo '<h2>'.get_the_title().'</h2>';
                            }
                        }
                    }
                    ?>
                    <?php politist_breadcrumbs(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>