<?php 

if(!function_exists('politist_setup')):

    function politist_setup()
    {
        //Textdomain
        load_theme_textdomain( 'politist', get_template_directory() . '/languages' );
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'politist-large', 1140, 670, true ); 
        add_image_size( 'politist-medium', 554, 554, true ); //use politist
        add_image_size( 'politist-small', 96, 96, true ); //use politist
        add_theme_support( 'post-formats', array( 'audio','gallery','image','link','quote','video' ) );
        add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form' ) );
        add_theme_support( 'automatic-feed-links' );

        add_editor_style('');

        if ( ! isset( $content_width ) )
        $content_width = 660;
    }

    add_action('after_setup_theme','politist_setup');

endif;


if(!function_exists('politist_pagination')):

    function politist_pagination( $page_numb , $max_page )
    {  
        $big = 999999999; // need an unlikely integer
        echo '<div class="themeum-pagination">';
        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => $page_numb,
            'prev_text'          => ('<i class="fa fa-long-arrow-left"></i>'),
            'next_text'          => ('<i class="fa fa-long-arrow-right"></i>'),
            'total' => $max_page,
            'type'               => 'list',
        ) );
        echo '</div>';
    }
endif;


/*-------------------------------------------------------
 *              Themeum Comment
 *-------------------------------------------------------*/

if(!function_exists('politist_comment')):

    function politist_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        switch ( $comment->comment_type ) :
            case 'pingback' :
            case 'trackback' :
            // Display trackbacks differently than normal comments.
        ?>
        <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            
        <?php
            break;
            default :
            global $post;
        ?>
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
            <div id="comment-<?php comment_ID(); ?>" class="comment-body media">
                
                    <div class="comment-avartar pull-left">
                        <?php
                            echo get_avatar( $comment, $args['avatar_size'] );
                        ?>
                    </div>
                    <div class="comment-context media-body">
                        <div class="comment-head">
                            <?php
                                printf( '<span class="comment-author">%1$s</span>',
                                    get_comment_author_link());
                            ?>
                            <span class="comment-date"><?php echo get_comment_date('d / m / Y') ?></span>

                            <?php edit_comment_link( esc_html__( 'Edit', 'politist' ), '<span class="edit-link">', '</span>' ); ?>
                            <span class="comment-reply">
                                <?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'politist' ), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                            </span>
                        </div>

                        <?php if ( '0' == $comment->comment_approved ) : ?>
                        <p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'politist' ); ?></p>
                        <?php endif; ?>

                        <div class="comment-content">
                            <?php comment_text(); ?>
                        </div>
                    </div>
                
            </div>
        <?php
            break;
        endswitch; 
    }

endif;


/*-------------------------------------------------------
*           Themeum Breadcrumb
*-------------------------------------------------------*/
if(!function_exists('politist_breadcrumbs')):
function politist_breadcrumbs(){ ?>
    <ol class="breadcrumb">
        <li><a href="<?php echo esc_url(site_url()); ?>" class="breadcrumb_home"><?php esc_html_e('Home', 'politist') ?></a></li>
        <?php
            if(function_exists('is_product')){
                $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
                if(is_product()){
                    echo "<li><a href='".$shop_page_url."'>".__( 'shop','politist' )."</a></li>";
                }   
            }
        ?>
        <li class="active">
            <?php if(function_exists('is_shop')){ if(is_shop()){ _e('shop','politist'); } } ?>

            <?php if( is_tag() ) { ?>
            <?php esc_html_e('Posts Tagged ', 'politist') ?><span class="raquo">/</span><?php single_tag_title(); echo('/'); ?>
            <?php } elseif (is_day()) { ?>
            <?php esc_html_e('Posts made in', 'politist') ?> <?php the_time('F jS, Y'); ?>
            <?php } elseif (is_month()) { ?>
            <?php esc_html_e('Posts made in', 'politist') ?> <?php the_time('F, Y'); ?>
            <?php } elseif (is_year()) { ?>
            <?php esc_html_e('Posts made in', 'politist') ?> <?php the_time('Y'); ?>
            <?php } elseif (is_search()) { ?>
            <?php esc_html_e('Search results for', 'politist') ?> <?php the_search_query() ?>
            <?php } elseif (is_single()) { ?>
            <?php $category = get_the_category();
            if ( $category ) { 
                $catlink = get_category_link( $category[0]->cat_ID );
                echo ('<a href="'.esc_url($catlink).'">'.esc_html($category[0]->cat_name).'</a> '.'<span class="raquo"> /</span> ');
            }
            echo get_the_title(); ?>
            <?php } elseif (is_category()) { ?>
            <?php single_cat_title(); ?>
            <?php } elseif (is_tax()) { ?>
            <?php 
            $themeum_taxonomy_links = array();
            $themeum_term = get_queried_object();
            $themeum_term_parent_id = $themeum_term->parent;
            $themeum_term_taxonomy = $themeum_term->taxonomy;

            while ( $themeum_term_parent_id ) {
                $themeum_current_term = get_term( $themeum_term_parent_id, $themeum_term_taxonomy );
                $themeum_taxonomy_links[] = '<a href="' . esc_url( get_term_link( $themeum_current_term, $themeum_term_taxonomy ) ) . '" title="' . esc_attr( $themeum_current_term->name ) . '">' . esc_html( $themeum_current_term->name ) . '</a>';
                $themeum_term_parent_id = $themeum_current_term->parent;
            }

            if ( !empty( $themeum_taxonomy_links ) ) echo implode( ' <span class="raquo">/</span> ', array_reverse( $themeum_taxonomy_links ) ) . ' <span class="raquo">/</span> ';
                echo esc_html( $themeum_term->name ); 
            } elseif (is_author()) { 
                global $wp_query;
                $curauth = $wp_query->get_queried_object();

                esc_html_e('Posts by ', 'politist'); echo ' ',$curauth->nickname; 
            } elseif (is_page()) { 
                echo get_the_title(); 
            } elseif (is_home()) { 
                esc_html_e('Blog', 'politist');
            }
            ?>  
        </li>
    </ol>

<?php
}
endif;


/*-----------------------------------------------------
 *              Coming Soon Page Settings
 *----------------------------------------------------*/
if ( politist_options('comingsoon-en') ) {
    if(!function_exists('politist_my_page_template_redirect')):
        function politist_my_page_template_redirect()
        {
            if( is_page( ) || is_home() || is_category() || is_single() )
            { 
                if( !is_super_admin( get_current_user_id() ) ){
                    get_template_part( 'coming','soon');
                    exit();
                }
            }
        }
        add_action( 'template_redirect', 'politist_my_page_template_redirect' );
    endif;

    if(!function_exists('politist_cooming_soon_wp_title')):
        function politist_cooming_soon_wp_title(){
            return 'Coming Soon';
        }
        add_filter( 'wp_title', 'politist_cooming_soon_wp_title' );
    endif;
}

// woocommerce Cart Button

add_action( 'woocommerce_after_shop_loop_item', 'politist_add_more_info_buttons', 1 );
function politist_add_more_info_buttons() {
    add_action( 'woocommerce_after_shop_loop_item', 'politist_more_info_button' );
}
function politist_more_info_button() {
    global $product;
    echo '<a href="' . get_permalink( $product->id ) . '" class="button add_to_cart_button product_type_external">Add to Cart</a>';
}



if(!function_exists('politist_css_generator')){
    function politist_css_generator(){

        $output = '';

        /* *******************************
        **********      Preset   **********
        ********************************** */
        if( politist_options('preset') ){
            $link_color = esc_attr(politist_options('link-color'));

            if( politist_options('custom-preset-en') ) {

                if(isset($link_color)){
                    $output .= 'a,a:focus,.top-user-login i,.themeum-title .title-link:hover,.sub-title .breadcrumb>.active,.modal-content .lost-pass:hover,.home-two-crousel .carousel-control:hover,
            #mobile-menu ul li:hover > a,#mobile-menu ul li.active > a,.top-align .sp-moview-icon-user,#sidebar .widget ul li a:hover,.continue:hover:after,.common-menu-wrap .nav>li>ul li.active > a,.common-menu-wrap .nav>li>ul li:hover > a,
            .themeum-pagination ul li .current,.themeum-pagination ul li a:hover,.woocommerce div.product p.price, .woocommerce div.product span.price,
            .woocommerce .products .product:hover .product-content-inner h2,.entry-title.blog-entry-title a:hover,.woocommerce table.shop_table td a:hover,.common-menu-wrap .nav>li>a:hover,.small-news h4 a:hover,.news-wrap h3 a:hover,.footer-menu li a:hover,.tribe-events-list-separator-month span,.tribe-event-schedule-details:before,.tribe-events-venue-details:before,.tribe-events-month .tribe-events-page-title,.btn-blog,
            .singnature a:hover,.home-one-crousel .carousel-control:hover,#sidebar h3.widget_title span,.news-feed-wrap a:hover,.meta-date,.product-details h4 a:hover,#product-crousel.owl-theme .owl-controls .owl-nav div:hover,#videoPlay:hover,
            .timeline-description h6,.news-post h3 a:hover,.news-post > a,.widget-title,.widget ul li a:hover,.widget-post h5,.widget-post a:hover,
            .woocommerce .comment-text .star-rating span,.woocommerce form .form-row.woocommerce-invalid label,.tribe-events-list .type-tribe_events h2 a:hover { color: '. esc_attr($link_color) .'; }';
                }   

                if(isset($link_color)){
                    $output .= '.error-page-inner a.btn.btn-primary.btn-lg,.btn.btn-primary,.news-feed .news-feed-item .news-feed-info .meta-category a:hover,
            .spotlight-post .list-inline>li>a:after,input[type=submit],.form-submit input[type=submit],
            .widget .tagcloud a:hover,.carousel-left:hover, .carousel-right:hover,input[type=button],
            .woocommerce div.product form.cart .button,.woocommerce #respond input#submit,.woocommerce a.button, .woocommerce button.button, 
            .woocommerce input.button,.widget.widget_search #searchform .btn-search,#tribe-events .tribe-events-button, 
            #tribe-events .tribe-events-button:hover, #tribe_events_filters_wrapper input[type=submit], .tribe-events-button, 
            .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, .tribe-events-button:hover, 
            .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-], 
            .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a,.woocommerce a.button.alt,.woocommerce input.button.alt,
            .woocommerce .woocommerce-message,.filled-button:hover,.filled-button:focus,.news-img:hover .news-date:before,
            .featured-wrap:hover .news-date:before,.single-social-info.user,.single-social-info:hover,.single-social-info:focus,
            .common-menu-wrap .nav>li>ul li:hover > a:before,.common-menu-wrap .nav>li>ul li.active > a:before,.filled-button.user,
            .filled-button:hover,.filled-button:focus,.coming-soon-form button,.woocommerce .product-thumbnail-outer-inner .addtocart-btn a.button:hover,#resume-carousel .carousel-indicators li.active:before,#resume-carousel .carousel-indicators li:hover:before,.timeline-description:hover:before,.timeline-date:hover:after,.error-page-inner a.btn.btn-primary.btn-lg:hover, .btn.btn-primary:hover,.widget ul li a:before,a.tags.active,a.tags:hover,.woocommerce .site-content .single-product-details .cart .button:hover,.woocommerce #review_form #respond .comment-form .form-submit input:hover,.widget table thead tr th,ul.tribe-events-sub-nav li a,#tribe-bar-form .tribe-bar-submit input[type=submit]:hover{background-color: '. esc_attr($link_color) .'; }';
                }   
                if(isset($link_color)){
                    $output .= 'input:focus, textarea:focus, keygen:focus, select:focus,.woocommerce form .form-row.woocommerce-invalid .select2-container, .woocommerce form .form-row.woocommerce-invalid input.input-text, .woocommerce form .form-row.woocommerce-invalid select { border-color: '. esc_attr($link_color) .'; }';
                }

            }

            if( politist_options('custom-preset-en') ) {
                
                if( politist_options('hover-color') ){
                    $output .= 'a:hover, .widget.widget_rss ul li a{ color: '.esc_attr( politist_options('hover-color') ) .'; }';
                    $output .= '.error-page-inner a.btn.btn-primary.btn-lg:hover,.btn.btn-primary:hover,input[type=button]:hover,
                    .woocommerce div.product form.cart .button:hover,.woocommerce #respond input#submit:hover,
                    .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
                    .widget.widget_search #searchform .btn-search:hover,.woocommerce a.button.alt:hover,
                    .woocommerce input.button.alt:hover,.btn-block.submit_button:hover{ background-color: '.esc_attr( politist_options('hover-color') ) .'; }';
                    $output .= '.btn.btn-primary{ border-color: '.esc_attr( politist_options('hover-color') ) .'; }';
                }
            }

        } else {
        $output ='a,a:focus,.small-news h4 a:hover,.top-user-login i,.themeum-title .title-link:hover,.sub-title .breadcrumb>.active,.modal-content .lost-pass:hover,.common-menu-wrap .nav>li>ul li.active > a,.common-menu-wrap .nav>li>ul li:hover > a,
            #mobile-menu ul li:hover > a,#mobile-menu ul li.active > a,.top-align .sp-moview-icon-user,#sidebar .widget ul li a:hover,.continue:hover:after,.btn-blog,.singnature a:hover,.home-one-crousel .carousel-control:hover,
            .themeum-pagination ul li .current,.themeum-pagination ul li a:hover,.woocommerce div.product p.price, .woocommerce div.product span.price,
            .woocommerce .products .product:hover .product-content-inner h2,.entry-title.blog-entry-title a:hover,.woocommerce table.shop_table td a:hover,.common-menu-wrap .nav>li>a:hover,.news-wrap h3 a:hover,.footer-menu li a:hover,.tribe-events-list-separator-month span,.tribe-event-schedule-details:before,.tribe-events-venue-details:before,.tribe-events-month .tribe-events-page-title,.home-two-crousel .carousel-control:hover,
            #sidebar h3.widget_title span,.news-feed-wrap a:hover,.meta-date,.product-details h4 a:hover,#product-crousel.owl-theme .owl-controls .owl-nav div:hover,#videoPlay:hover,.timeline-description h6,.news-post h3 a:hover,.news-post > a,.widget-title,.widget ul li a:hover,.widget-post h5,.widget-post a:hover,.woocommerce .comment-text .star-rating span,.woocommerce form .form-row.woocommerce-invalid label,
            .tribe-events-list .type-tribe_events h2 a:hover{ color: #ed1c24; }.error-page-inner a.btn.btn-primary.btn-lg,.btn.btn-primary,.news-feed .news-feed-item .news-feed-info .meta-category a:hover,
            .spotlight-post .list-inline>li>a:after,input[type=submit],.form-submit input[type=submit],
            .widget .tagcloud a:hover,.carousel-left:hover, .carousel-right:hover,input[type=button],
            .woocommerce div.product form.cart .button,.woocommerce #respond input#submit,.woocommerce a.button, .woocommerce button.button, 
            .woocommerce input.button,.widget.widget_search #searchform .btn-search,#tribe-events .tribe-events-button, 
            #tribe-events .tribe-events-button:hover, #tribe_events_filters_wrapper input[type=submit], .tribe-events-button, 
            .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, .tribe-events-button:hover, 
            .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-], 
            .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a,.woocommerce a.button.alt,.woocommerce input.button.alt,
            .woocommerce .woocommerce-message,.news-img:hover .news-date:before,.featured-wrap:hover .news-date:before,.single-social-info.user,.single-social-info:hover,.single-social-info:focus,.common-menu-wrap .nav>li>ul li:hover > a:before,
            .common-menu-wrap .nav>li>ul li.active > a:before,.filled-button.user,.filled-button:hover,.filled-button:focus,
            .coming-soon-form button,.woocommerce .product-thumbnail-outer-inner .addtocart-btn a.button:hover,#resume-carousel .carousel-indicators li.active:before,#resume-carousel .carousel-indicators li:hover:before,.timeline-description:hover:before,.timeline-date:hover:after,.error-page-inner a.btn.btn-primary.btn-lg:hover, .btn.btn-primary:hover,.widget ul li a:before,a.tags.active,a.tags:hover,.woocommerce .site-content .single-product-details .cart .button:hover,.woocommerce #review_form #respond .comment-form .form-submit input:hover,.widget table thead tr th,ul.tribe-events-sub-nav li a,#tribe-bar-form .tribe-bar-submit input[type=submit]:hover{ background-color: #ed1c24; }
            input:focus, textarea:focus, keygen:focus, select:focus  { border-color: #ed1c24; }a:hover, .widget.widget_rss ul li a{ color: #ed0007; }.error-page-inner a.btn.btn-primary.btn-lg:hover,.btn.btn-primary:hover,input[type=button]:hover,
            .woocommerce div.product form.cart .button:hover,.woocommerce #respond input#submit:hover,
            .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
            .widget.widget_search #searchform .btn-search:hover,.woocommerce a.button.alt:hover,
            .woocommerce input.button.alt:hover,.btn-block.submit_button:hover,.filled-button:hover,.filled-button:focus,,input[type=submit]:hover{ background-color: #ed0007; }
            .btn.btn-primary,.woocommerce form .form-row.woocommerce-invalid .select2-container, 
.woocommerce form .form-row.woocommerce-invalid input.input-text, 
.woocommerce form .form-row.woocommerce-invalid select{ border-color: #ed0007; }';
        }



/* =================================================
* ================  Quick Style ====================
==================================================== */


        # Topbar section start.
        if(politist_options('topbar-bg')){
            $output .= '.topbar{ background-color: '.esc_attr( politist_options('topbar-bg') ) .'; }';
        }
        if(politist_options('topbar-color')){
            $output .= '.social-icon ul li a, .menu-social .top-align a{ color: '.esc_attr( politist_options('topbar-color') ) .'; }';
        } 
        if(politist_options('topbar-link-color')){
            $output .= '.social-icon ul li a:hover, .menu-social .top-align a:hover{ color: '.esc_attr( politist_options('topbar-link-color') ) .'; }';
        } 
        if(politist_options('topbar-padding-top')){
            $output .= '.topbar{ padding-top: '. (int) esc_attr( politist_options('topbar-padding-top') ) .'px; }';
        }
        if(politist_options('topbar-padding-bottom')){
            $output .= '.topbar{ padding-bottom: '. (int) esc_attr( politist_options('topbar-padding-bottom') ) .'px; }';
        }
        # Topbar section end.


        # Mainmenu section, add background, color, padding.
        $menu_bg = esc_attr(politist_options('menu_bg'));
        if(isset($menu_bg)){
            $output .= '.header-menulogocenter .main-menu-wrap, .main-menu-wrap, .site-header{ background-color: '.esc_attr( politist_options('menu_bg') ) .' !important; }';
        }  
        $menu_font_color = esc_attr(politist_options('menu_font_color'));
        if(isset($menu_font_color)){
            $output .= '.common-menu-wrap .nav>li>a{color: '.esc_attr( politist_options('menu_font_color') ) .'; }';
        } 
        if(politist_options('menu-hover-color')){
            $output .= '.common-menu-wrap .nav>li>a:hover { color: '. esc_attr( politist_options('menu-hover-color') ) .'}';            
            $output .= '.common-menu-wrap .nav>li>a:hover:before { background-color: '. esc_attr( politist_options('menu-hover-color') ) .'}';
        }
        if(politist_options('manu-padding-top')){
            $output .= '.common-menu{ padding-top: '. (int) esc_attr( politist_options('topbar-padding-top') ) .'px; }';
        }
        if(politist_options('menu-padding-bottom')){
            $output .= '.common-menu{ padding-bottom: '. (int) esc_attr( politist_options('topbar-padding-bottom') ) .'px; }';
        }

            // submenu
            $submenu_bg = esc_attr(politist_options('submenu_bg'));
            if(isset($submenu_bg)){
                $output .= '.common-menu-wrap .nav>li ul{ background-color: '.esc_attr( politist_options('submenu_bg') ) .'; }';
            }  

            if(politist_options('submenu-color')){
                $output .= '.common-menu-wrap .nav>li>ul li a { color: '. esc_attr( politist_options('submenu-color') ) .'}';
            }  

            if(politist_options('submenu-hover-color')){
                $output .= '.common-menu-wrap .nav>li>ul li a:hover { color: '. esc_attr( politist_options('submenu-hover-color') ) .'}';
                $output .= '.common-menu .common-menu-wrap .nav>li .sub-menu>li a:hover:before{ background-color: '. esc_attr( politist_options('submenu-hover-color') ) .'}';
            } 
        # end mainmenu

        # Banner section: add title, title color, subtitle, subtitle color
        if(politist_options('banner_title_color')){
            $output .= '.breadcrumbs-area .bread-content h2 { color: '. esc_attr( politist_options('banner_title_color') ) .'}';
        }
        if( politist_options('banner_title_size') ){
            $output .= '.breadcrumbs-area .bread-content h2{ font-size: '. (int) esc_attr( politist_options('banner_title_size')) .'px; }';
        }  
        if(politist_options('banner_subtitle_color')){
            $output .= '.breadcrumbs-area .breadcrumb>li a, .breadcrumb>li+li:before, .breadcrumbs-area .breadcrumb>.active { color: '. esc_attr( politist_options('banner_subtitle_color') ) .'}';
        }
        if( politist_options('banner_subtitle_size') ){
            $output .= '.breadcrumb{ font-size: '. (int) esc_attr( politist_options('banner_subtitle_size')) .'px; }';
        }
        # end banner

        # logo & favicon
        if( politist_options('logo-width') ){
            $output .= '.logo-wrapper img{ width: '. (int) esc_attr( politist_options('logo-width')) .'px; }';
        }  

        if( politist_options('logo-height') ){
            $output .= '.logo-wrapper img{ height: '. (int) esc_attr( politist_options('logo-height')) .'px; }';
        } 
        # end logo & favicon

        # Footer padding

        $footer_text_color = esc_attr(politist_options('footer_text_color'));
        if(isset($footer_text_color)){
            $output .= '.footer-area .copy-right, .footer-area .footer-menu li a, .footer-two-area .copy-right, .footer-two-area, .footer-menu li a{ color: '.esc_attr( politist_options('footer_text_color') ) .'; }';
        } 

        $footer_link_color = esc_attr(politist_options('footer_link_color')); 
        if(isset($footer_link_color)){
            $output .= '.footer-area .footer-menu li a:hover, .footer-two-area .footer-menu li a:hover, .footer-two-area .social-icon li a:hover i, .footer-area .social-icon li a:hover i{ color: '.esc_attr( politist_options('footer_link_color') ) .' !important ; }';
        } 
        if(politist_options('footer-padding-top')){
           $output .= '.footer-two-area{ padding-top: '. (int) esc_attr( politist_options('footer-padding-top') ) .'px; }';
        }        
        if(politist_options('footer-padding-bottom')){
           $output .= '.footer-two-area{ padding-bottom: '. (int) esc_attr( politist_options('footer-padding-bottom') ) .'px; }';
        }
        # end Footer section.

        $headerbg = politist_options('header-bg');
        if(isset($headerbg)){
            $output .= '.header{ background: '.esc_attr(politist_options('header-bg')) .'; }';
        }

        if (politist_options('header-fixed')){

            if(isset($headerbg)){
                $output .= '#masthead.sticky{ background-color: rgba(255,255,255,.97); }';
            }
            $output .= 'header.sticky{ position:fixed;top:0; z-index:99;margin:0 auto 30px; width:100%;box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.22);}';
            $output .= 'header.sticky #header-container{ padding:0;transition: padding 200ms linear; -webkit-transition:padding 200ms linear;}';
            $output .= 'header.sticky .navbar.navbar-default{ background: rgba(255,255,255,.95); border-bottom:1px solid #f5f5f5}';
        }


        if( politist_options('header-padding-top') ){
            $output .= '.site-header{ padding-top: '. (int) esc_attr( politist_options('header-padding-top')) .'px; }';
        }

        if(politist_options('header-height')){
            $output .= '.site-header{ min-height: '. (int) esc_attr(politist_options('header-height')) .'px; }';
        }

        if(politist_options('header-padding-bottom')){
            $output .= '.site-header{ padding-bottom: '. (int) esc_attr( politist_options('header-padding-bottom') ) .'px; }';
        }

        if(politist_options('footer-background')){
            $output .= '.footer-area, .footer-two-area{ background: '.esc_attr(politist_options('footer-background')) .'; }';
        }

        if (politist_options('comingsoon-en')) {
            $output .= "body {
                background: #fff;
                display: table;
                width: 100%;
                height: 100%;
                min-height: 100%;
            }";
        }


        if ( politist_options('errorbg') ) {
            $output .= "body.error404,BODY.page-template-404{
                width: 100%;
                height: 100%;
                min-height: 100%;
                background-size: cover;
            }";
        }
        if( politist_options('custom-css') ) {
            $output .= politist_options('custom-css');
        }

        $output = '<style>'.$output.'</style>';

        echo $output;
    }
}
add_action('wp_head', 'politist_css_generator');

