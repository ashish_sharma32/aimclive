<?php 
/*-------------------------------------------*
 *      Themeum Widget Registration
 *------------------------------------------*/

if(!function_exists('politist_widdget_init')):

    function politist_widdget_init()
    {

        register_sidebar(array( 'name'          => esc_html__( 'Sidebar', 'politist' ),
                                'id'            => 'sidebar',
                                'description'   => esc_html__( 'Widgets in this area will be shown on Sidebar.', 'politist' ),
                                'before_title'  => '<h3 class="widget_title">',
                                'after_title'   => '</h3>',
                                'before_widget' => '<div id="%1$s" class="widget %2$s" >',
                                'after_widget'  => '</div>'
                    )
        );
        
        global $woocommerce;
        if($woocommerce) {
            register_sidebar(array(
                'name'          => __( 'Shop', 'politist' ),
                'id'            => 'shop',
                'description'   => __( 'Widgets in this area will be shown on Shop Sidebar.', 'politist' ),
                'before_title'  => '<div class="widget_title"><h3 class="widget_title">',
                'after_title'   => '</h3></div>',
                'before_widget' => '<div id="%1$s" class="widget %2$s" >',
                'after_widget'  => '</div>'
                )
            );
        }   

    }
    
    add_action('widgets_init','politist_widdget_init');

endif;


/*-------------------------------------------*
 *      Themeum Style
 *------------------------------------------*/

if(!function_exists('politist_style')):

    function politist_style(){

        wp_enqueue_media();
        wp_enqueue_style('thm-style',get_stylesheet_uri());
        wp_enqueue_script('bootstrap',POLITIST_JS.'bootstrap.min.js',array(),false,true);       
        wp_enqueue_script('jquery.prettySocial',POLITIST_JS.'jquery.prettySocial.min.js',array(),false,true);
        wp_enqueue_script('jquery.ajax.login',POLITIST_JS.'ajax-login-script.js',array(),false,true);

        global $themeum_options;
        if( isset($themeum_options['custom-preset-en']) && $themeum_options['custom-preset-en']==0 ) {
            wp_enqueue_style( 'themeum-preset', get_template_directory_uri(). '/css/presets/preset' . $themeum_options['preset'] . '.css', array(),false,'all' );       
        }
        wp_enqueue_script('main',POLITIST_JS.'main.js',array(),false,true);

    }

    add_action('wp_enqueue_scripts','politist_style');

endif;




if(!function_exists('politist_admin_style')):

    function politist_admin_style(){
        wp_enqueue_media();
        wp_register_script('thmpostmeta', get_template_directory_uri() .'/js/admin/post-meta.js');
        wp_enqueue_script('themeum-widget-js', get_template_directory_uri().'/js/widget-js.js', array('jquery'));
        wp_enqueue_script('thmpostmeta');
    }
    add_action('admin_enqueue_scripts','politist_admin_style');

endif;


/*-------------------------------------------------------
*           Include the TGM Plugin Activation class
*-------------------------------------------------------*/

require_once( get_template_directory()  . '/lib/class-tgm-plugin-activation.php');

add_action( 'tgmpa_register', 'politist_plugins_include');

if(!function_exists('politist_plugins_include')):

    function politist_plugins_include()
    {
        $plugins = array(

                array(
                    'name'                  => 'Themeum Core',
                    'slug'                  => 'themeum-core',
                    'source'                => 'http://demo.themeum.com/wordpress/plugins/politist/themeum-core.zip',
                    'required'              => true,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ), 
                array(
                    'name'                  => 'WPBakery Visual Composer',
                    'slug'                  => 'js_composer',
                    'source'                => 'http://demo.themeum.com/wordpress/plugins/js_composer.zip',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),
                array(
                    'name'                  => 'revslider',
                    'slug'                  => 'revslider',
                    'source'                => 'http://demo.themeum.com/wordpress/plugins/revslider.zip',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),                 
                array(
                    'name'                  => 'Themeum Tweet',
                    'slug'                  => 'themeum-tweet',
                    'source'                => 'http://demo.themeum.com/wordpress/plugins/politist/themeum-tweet.zip',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),                                               
                array(
                    'name'                  => 'MailChimp for WordPress',
                    'slug'                  => 'mailchimp-for-wp',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/mailchimp-for-wp.3.1.4.zip',
                ),   
                array(
                    'name'                  => 'Woocoomerce',
                    'slug'                  => 'woocommerce',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/woocommerce.3.0.4.zip', 
                ),                                                                            
                array(
                    'name'                  => 'Widget Importer Exporter',
                    'slug'                  => 'widget-importer-exporter',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/widget-importer-exporter.1.4.5.zip',
                ),
                array(
                    'name'                  => 'Contact Form 7',
                    'slug'                  => 'contact-form-7',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/contact-form-7.4.4.zip',
                ),                
                array(
                    'name'                  => 'the events calendar',
                    'slug'                  => 'the-events-calendar',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/the-events-calendar.4.1.3.zip',
                ),
            );
    $config = array(
            'domain'            => 'politist',           
            'default_path'      => '',                           
            'parent_menu_slug'  => 'themes.php',                 
            'parent_url_slug'   => 'themes.php',                
            'menu'              => 'install-required-plugins',   
            'has_notices'       => true,                         
            'is_automatic'      => false,                      
            'message'           => '',                     
            'strings'           => array(
                        'page_title'                                => esc_html__( 'Install Required Plugins', 'politist' ),
                        'menu_title'                                => esc_html__( 'Install Plugins', 'politist' ),
                        'installing'                                => esc_html__( 'Installing Plugin: %s', 'politist' ), 
                        'oops'                                      => esc_html__( 'Something went wrong with the plugin API.', 'politist'),
                        'return'                                    => esc_html__( 'Return to Required Plugins Installer', 'politist'),
                        'plugin_activated'                          => esc_html__( 'Plugin activated successfully.','politist'),
                        'complete'                                  => esc_html__( 'All plugins installed and activated successfully. %s', 'politist' ) 
                )
    );

    tgmpa( $plugins, $config );

    }

endif;