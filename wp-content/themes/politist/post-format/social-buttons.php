<?php
$permalink = get_permalink(get_the_ID());
$titleget = get_the_title();
$tw_username = '';
$tw_username = politist_options('twitter-username');
$media_url ='';
if( has_post_thumbnail( get_the_ID() ) ){
    $thumb_src =  wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); 
    $media_url = $thumb_src[0];
}
?>

<a class="prettySocial single-social-info facebook" href="#" data-type="facebook" data-url="<?php echo esc_url($permalink); ?>" data-title="<?php echo $titleget ?>" data-description="<?php echo $titleget ?>" data-media="<?php echo esc_url( $media_url ); ?>"><i class="fa fa-facebook-square"></i><?php _e('Share With Facebook','politist');?></a>
<a class="prettySocial single-social-info twitter" href="#" data-type="twitter" data-url="<?php echo esc_url($permalink); ?>" data-description="<?php echo $titleget ?>" data-via="<?php echo $tw_username; ?>"><i class="fa fa-twitter"></i><?php _e('Tweet With Twitter','politist');?></a>

