<?php global $themeum_options; ?>

<div class="entry-blog">
    <div class="entry-headder">
        <h2 class="entry-title blog-entry-title">
            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            <?php if ( is_sticky() && is_home() && ! is_paged() ) { ?>
            <sup class="featured-post"><i class="fa fa-star-o"></i><?php esc_html_e( 'Sticky', 'politist' ) ?></sup>
            <?php } ?>
        </h2> <!-- //.entry-title --> 
    </div>
    <?php if ( politist_options('blog-author') ) { ?>
        <?php if ( get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "" ) { ?>
            <span class="meta-author"><?php esc_html_e( 'By', 'politist' ) ?> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author_meta('first_name');?> <?php echo get_the_author_meta('last_name');?></a></span>
        <?php } else { ?>
            <span class="meta-author"><?php esc_html_e( 'By', 'politist' ) ?> <?php the_author_posts_link() ?></span>
        <?php }?>
    <?php }?>  

    <?php if ( politist_options('blog-category') ) { ?>
        <span class="meta-category"><?php esc_html_e( 'Category: ', 'politist' ) ?> <?php echo get_the_category_list(', '); ?></span>
    <?php }?> 

    <?php if ( politist_options('blog-comment') ) { ?>
        <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
            <span class="comments-in">
                <?php _e('Comments: ','politist');?>
                <?php comments_popup_link( '<span class="leave-reply">' . esc_html__( 'No comment', 'politist' ) . '</span>', esc_html__( 'One comment', 'politist' ), esc_html__( '% comments', 'politist' ) ); ?>
            </span>
        <?php endif; //.comment-link ?>
    <?php }?> 

    <?php if ( politist_options('blog-tag') ) { ?>
        <span class="tags-in"><?php _e('Tags: ','politist');?> <?php the_tags('', ', ', '<br />'); ?></span>
    <?php }?> 
    
    <div class="entry-summary clearfix">
        <?php if ( is_single() ) {
            the_content();
        } else {
            echo politist_excerpt_max_charlength(300);
            if ( isset($themeum_options['blog-continue-en']) && $themeum_options['blog-continue-en']==1 ) {
                if ( isset($themeum_options['blog-continue']) && $themeum_options['blog-continue'] ) {
                    $continue = esc_html($themeum_options['blog-continue']);
                    echo '<p class="wrap-btn-style"><a class="btn-blog" href="'.get_permalink().'">'. $continue .' <i class="fa fa-long-arrow-right"></i></a></p>';
                } else {
                    echo '<p class="wrap-btn-style"><a class="btn-blog" href="'.get_permalink().'">'. esc_html__( 'Read More', 'politist' ) .' <i class="fa fa-long-arrow-right"></i></a></p>';
                } 
            }
     
        } 
        wp_link_pages( array(
            'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'politist' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
        ) );
        ?>
    </div> <!-- //.entry-summary -->
</div> <!--/.entry-meta -->



